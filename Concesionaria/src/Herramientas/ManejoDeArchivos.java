/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herramientas;

import concesionaria.Mantenimientos.Mantenimiento;
import concesionaria.Solicitudes.Solicitud;
import concesionaria.Usuarios.Usuario;
import concesionaria.Vehiculos.Vehiculo;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;
import javafx.application.Platform;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class ManejoDeArchivos {

    static final Logger LOGGER = Logger.getAnonymousLogger();
    private static final String filenameUsuarios = "src/PersistenciaDeDatos/users.dat";
    private static final String filenameVehiculos = "src/PersistenciaDeDatos/vehiculos.dat";
    private static final String filenameSolicitudes = "src/PersistenciaDeDatos/solicitudes.dat";
    private static final String filenameMantenimientos = "src/PersistenciaDeDatos/mantenimientos.dat";

    /**
     * Lee un archivo binario y retorna una arreglo de usuarios
     *
     * @return Una arreglo de usuarios
     */
    public synchronized static ArrayList<Usuario> cargarUsuarios() {
        ArrayList<Usuario> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(filenameUsuarios))) {
            try {
                while (true) {
                    z = (ArrayList<Usuario>) objInputStream.readObject();
                    //System.out.println(z);
                }
            } catch (EOFException eof) {
                System.out.println(Color.ANSI_GREEN + "Se cargaron los usuarios...");
            } catch (ClassNotFoundException ex) {
                System.out.println("Corrupted file");
            }

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    /**
     * Sobree escibre un archivo binario con una arreglo de Usuarios
     *
     * @param ListaUsuarios la lista a escribirse
     */
    public synchronized static void guardarUsuarios(ArrayList<Usuario> ListaUsuarios) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(filenameUsuarios))) {
            objOutputStream.writeObject(ListaUsuarios);
            System.out.println("... written to users.dat.");

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
        }
    }

    public synchronized static ArrayList<Vehiculo> cargarVehiculos() {
        ArrayList<Vehiculo> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(filenameVehiculos))) {
            try {
                while (true) {
                    z = (ArrayList<Vehiculo>) objInputStream.readObject();
                    //System.out.println(z.toString());
                }
            } catch (EOFException eof) {
                System.out.println(Color.ANSI_GREEN + "Se cargaron los Vehiculos...");
            } catch (ClassNotFoundException ex) {
                System.out.println("Corrupted file");
            }

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    public synchronized static void guardarVehiculos(ArrayList<Vehiculo> ListaVehiculos) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(filenameVehiculos))) {
            objOutputStream.writeObject(ListaVehiculos);
            System.out.println("... written to vehiculos.dat.");

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
        }
    }

    public synchronized static ArrayList<Solicitud> cargarSolicitudes() {
        ArrayList<Solicitud> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(filenameSolicitudes))) {
            try {
                while (true) {
                    z = (ArrayList<Solicitud>) objInputStream.readObject();
                    //System.out.println(z.toString());
                }
            } catch (EOFException eof) {
                System.out.println(Color.ANSI_GREEN + "Se cargaron las Solicitudes...");
            } catch (ClassNotFoundException ex) {
                System.out.println("Corrupted file");
            }

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    public synchronized static void guardarSolicitudes(ArrayList<Solicitud> ListaSolicitudes) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(filenameSolicitudes))) {
            objOutputStream.writeObject(ListaSolicitudes);
            System.out.println("... written to solicitudes.dat.");

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
        }
    }

    public synchronized static ArrayList<Mantenimiento> cargarMantenimientos() {
        ArrayList<Mantenimiento> z = new ArrayList<>();
        try (ObjectInputStream objInputStream = new ObjectInputStream(
                new FileInputStream(filenameMantenimientos))) {
            try {
                while (true) {
                    z = (ArrayList<Mantenimiento>) objInputStream.readObject();
                    //System.out.println(z.toString());
                }
            } catch (EOFException eof) {
                System.out.println(Color.ANSI_GREEN + "Se cargaron los mantenimientos...");
            } catch (ClassNotFoundException ex) {
                System.out.println("Corrupted file");
            }

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
            Platform.exit();
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
            Platform.exit();
        }
        return z;
    }

    public synchronized static void guardarMantenimientos(ArrayList<Mantenimiento> ListaMantenimientos) {
        try (ObjectOutputStream objOutputStream = new ObjectOutputStream(
                new FileOutputStream(filenameMantenimientos))) {
            objOutputStream.writeObject(ListaMantenimientos);
            System.out.println("... written to mantenimientos.dat.");

        } catch (FileNotFoundException e1) {
            System.err.println(e1.getMessage());
        } catch (IOException e2) {
            System.err.println(e2.getMessage());
        }
    }

    public static void LeerFichero() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File("src/PersistenciaDeDatos/datos.txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            // Lectura del fichero
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public static void EscribirFichero() {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("src/PersistenciaDeDatos/datos.txt");
            pw = new PrintWriter(fichero);

            for (int i = 0; i < 10; i++) {
                pw.println("Linea " + i);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

}
