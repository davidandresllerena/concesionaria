/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Herramientas.Color;
import Herramientas.ManejoDeArchivos;
import Herramientas.Validaciones;
import static Herramientas.Validaciones.isCorrectYear;
import static Herramientas.Validaciones.isNumeric;
import static Herramientas.Validaciones.validarContrasena;
import static Herramientas.Validaciones.validarOpcion;
import concesionaria.Concesionaria;
import concesionaria.Mantenimientos.Mantenimiento;
import concesionaria.Solicitudes.Compra;
import concesionaria.Solicitudes.Cotizacion;
import concesionaria.Solicitudes.Solicitud;
import concesionaria.Usuarios.Cliente;
import concesionaria.Usuarios.Empleado;
import concesionaria.Usuarios.JefeTaller;
import concesionaria.Usuarios.Supervisor;
import concesionaria.Usuarios.Usuario;
import concesionaria.Usuarios.Vendedor;
import concesionaria.Vehiculos.Automovil;
import concesionaria.Vehiculos.Camion;
import concesionaria.Vehiculos.Motocicleta;
import concesionaria.Vehiculos.Tractor;
import concesionaria.Vehiculos.Vehiculo;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Main {

    Cliente cliente;
    JefeTaller jefeTaller;
    Supervisor supervisor;
    Vendedor vendedor;
    Concesionaria SistemaActual = new Concesionaria();
    Scanner entradaPorTeclado = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        main.menuPrincipal();
    }

    public void menuPrincipal() throws IOException {
        System.out.println("");
        String opcion = "";
        while (!"2".equals(opcion)) {
            System.out.println("  ____ _____ ______ _   ___      ________ _   _ _____ _____   ____  \n"
                    + " |  _ \\_   _|  ____| \\ | \\ \\    / /  ____| \\ | |_   _|  __ \\ / __ \\ \n"
                    + " | |_) || | | |__  |  \\| |\\ \\  / /| |__  |  \\| | | | | |  | | |  | |\n"
                    + " |  _ < | | |  __| | . ` | \\ \\/ / |  __| | . ` | | | | |  | | |  | |\n"
                    + " | |_) || |_| |____| |\\  |  \\  /  | |____| |\\  |_| |_| |__| | |__| |\n"
                    + " |____/_____|______|_| \\_|   \\/   |______|_| \\_|_____|_____/ \\____/ \n"
                    + "                                                                   ");
            System.out.println(Color.ANSI_RED + "Menu Principal");
            System.out.println(Color.ANSI_BLUE + "1. Iniciar Sesion");
            System.out.println(Color.ANSI_BLUE + "2. Salir");
            System.out.println(Color.ANSI_RED + "Ingrese opcion:");
            opcion = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Iniciar Sesion");
                System.out.println("2. Salir");
                opcion = entradaPorTeclado.nextLine();
            }
            switch (opcion) {
                case "1":
                    iniciarSesion();
                    break;
                case "2":
                    System.out.println(Color.ANSI_RED + "Saliendo del programa...");
                    break;

            }
        }
    }

    private void iniciarSesion() throws IOException {
        boolean validacion = true;
        System.out.println("Ingrese su usuario: ");
        String correo = entradaPorTeclado.nextLine();
        System.out.println("Ingrese su contrasena: ");
        String contrasena = entradaPorTeclado.nextLine();
        for (Usuario us : SistemaActual.getListaUsuarios()) {
            if (us.getContrasena().equals(contrasena) && us.getUser().toLowerCase().equals(correo.toLowerCase())) {
                validacion = false;
                if (us instanceof Supervisor) {
                    Supervisor es = (Supervisor) us;
                    MenuSupervisor(es);
                } else if (us instanceof JefeTaller) {
                    JefeTaller co = (JefeTaller) us;
                    MenuJefeTaller(co);
                } else if (us instanceof Cliente) {
                    Cliente admin = (Cliente) us;
                    MenuCliente(admin);
                } else if (us instanceof Vendedor) {
                    Vendedor admin = (Vendedor) us;
                    MenuVendedor(admin);
                }
            }
        }
        if (validacion) {
            System.out.println(Color.ANSI_RED + "Correo y contrasenas incorrectos");
        }

    }

    public void MenuSupervisor(Supervisor supervisor) throws IOException {
        String opcion = "";
        while (!"7".equals(opcion)) {
            System.out.println("  __  __                     _____                             _                \n"
                    + " |  \\/  |                   / ____|                           (_)               \n"
                    + " | \\  / | ___ _ __  _   _  | (___  _   _ _ __   ___ _ ____   ___ ___  ___  _ __ \n"
                    + " | |\\/| |/ _ \\ '_ \\| | | |  \\___ \\| | | | '_ \\ / _ \\ '__\\ \\ / / / __|/ _ \\| '__|\n"
                    + " | |  | |  __/ | | | |_| |  ____) | |_| | |_) |  __/ |   \\ V /| \\__ \\ (_) | |   \n"
                    + " |_|  |_|\\___|_| |_|\\__,_| |_____/ \\__,_| .__/ \\___|_|    \\_/ |_|___/\\___/|_|   \n"
                    + "                                        | |                                     \n"
                    + "                                        |_|                                     ");
            System.out.println("1. Consultar los empleados.");
            System.out.println("2. Modificar datos de empleado.");
            System.out.println("3. Consultar cuántos vehículos ha vendido cada vendedor.");
            System.out.println("4. Consultar los vehículos que se encuentran en reparación.");
            System.out.println("5. Ingresar nuevos vehículos al stock.");
            System.out.println("6. Revisar solicitudes de compra.");
            System.out.println("7. Salir.");
            opcion = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion, 7)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Consultar los empleados.");
                System.out.println("2. Modificar datos de empleado.");
                System.out.println("3. Consultar cuántos vehículos ha vendido cada vendedor.");
                System.out.println("4. Consultar los vehículos que se encuentran en reparación.");
                System.out.println("5. Ingresar nuevos vehículos al stock.");
                System.out.println("6. Revisar solicitudes de compra.");
                System.out.println("7. Salir.");
                opcion = entradaPorTeclado.nextLine();
            }
            switch (opcion) {
                case "1":
                    verEmpleadosDelSistema();
                    break;
                case "2":
                    modificarEmpleado(SistemaActual.getListaUsuarios(), supervisor);
                    break;
                case "3":
                    supervisor.consultarVehiculosVendidosPorVendedor(SistemaActual.getListaUsuarios());
                    break;
                case "4":
                    supervisor.consultarVehiculosEnReparacion(SistemaActual.getListaMantenimientos());
                    break;
                case "5":
                    agregarVehiculo();
                    break;
                case "6":
                    System.out.println("Elija las solicitudes de compra que desea revisar: ");
                    int counterc = 0;
                    String revisarsolcompra = "";
                    for (Compra com : supervisor.getSolicitudesCompra()) {
                        counterc += 1;
                        System.out.println(Color.ANSI_PURPLE + counterc + ".- " + com.toStringCliente());
                    }
                    revisarsolcompra = entradaPorTeclado.nextLine();
                    while (!(Integer.valueOf(revisarsolcompra) > 0 && Integer.valueOf(revisarsolcompra) <= supervisor.getSolicitudesCompra().size())) {
                        System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                        System.out.println("Intente nuevamente...");
                        revisarsolcompra = entradaPorTeclado.nextLine();
                    }
                    System.out.println("¿Dese aceptar esta solicitud de compra?");
                    String aceptacionsoli = "";
                    String motivorech = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    System.out.println("3. Salir.");
                    aceptacionsoli = entradaPorTeclado.nextLine();
                    while (validarOpcion(aceptacionsoli, 3)) {
                        System.out.println(Color.ANSI_RED + "Elija una opción correcta!");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        System.out.println("3. Salir.");
                        aceptacionsoli = entradaPorTeclado.nextLine();
                    }
                    switch (aceptacionsoli) {
                        case "1":
                            SistemaActual.cambioEstadoSolicitudSi(supervisor, true, "Aprobada", revisarsolcompra);

                            //SistemaActual.cambioEstadoSolicitud(supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1), true, motivorech);
                            //supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1).getCliente().getVehiculosComprados().add(supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1).getVehiculo());
                            //SistemaActual.esMismoCliente(supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1).getCliente()).getVehiculosComprados().add(supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1).getVehiculo());
                            break;
                        case "2":
                            System.out.println("Ingrese el motivo del rechazo: ");
                            motivorech = entradaPorTeclado.nextLine();
                            SistemaActual.cambioEstadoSolicitudSi(supervisor, false, motivorech, revisarsolcompra);
                            //SistemaActual.cambioEstadoSolicituds(supervisor, true, motivorech, revisarsolcompra);
                            //SistemaActual.cambioEstadoSolicitud(supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1), false, motivorech);
                            //supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1).setEstaAprobada(false);
                            //supervisor.getSolicitudesCompra().get(Integer.valueOf(revisarsolcompra) - 1).setMotivoRechazo(motivorech);
                            break;
                        case "3":
                            break;
                    }
                    ManejoDeArchivos.guardarSolicitudes(SistemaActual.getListaSolicitudes());
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
                    break;
                case "7":
                    break;

            }
        }

    }

    public void verEmpleadosDelSistema() {
        for (Usuario user : SistemaActual.getListaUsuarios()) {
            if (user instanceof Empleado) {
                Empleado emp = (Empleado) user;
                if (emp instanceof Supervisor) {
                    System.out.println(Color.ANSI_PURPLE + "Supervisor.- " + "Id: " + emp.getId() + ", " + "Nombres:  " + emp.getUser() + ", " + emp.getApellidos() + ", Usuario: " + emp.getUser());
                } else if (emp instanceof JefeTaller) {
                    System.out.println(Color.ANSI_PURPLE + "Jefe De Taller.- " + "Id: " + emp.getId() + ", " + "Nombres:  " + emp.getUser() + ", " + emp.getApellidos() + ", Usuario: " + emp.getUser());
                } else {
                    System.out.println(Color.ANSI_PURPLE + "Vendedor.- " + "Id: " + emp.getId() + ", " + "Nombres:  " + emp.getUser() + ", " + emp.getApellidos() + ", Usuario: " + emp.getUser());
                }

            }
        }
    }

    public void agregarVehiculo() {
        String opcion1 = "";
        while (!"5".equals(opcion1)) {
            System.out.println("Elija el tipo de vehiculo que desea agregar");
            System.out.println("1. Automovil");
            System.out.println("2. Camion");
            System.out.println("3. Motocicleta");
            System.out.println("4. Tractor");
            System.out.println("5. Salir");
            opcion1 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion1, 5)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Automovil");
                System.out.println("2. Camion");
                System.out.println("3. Motocicleta");
                System.out.println("4. Tractor");
                System.out.println("5. Salir");
                opcion1 = entradaPorTeclado.nextLine();
            }
            switch (opcion1) {
                case "1":
                    System.out.println("Escriba la marca del automovil:");
                    String marca6 = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el modelo del automovil:");
                    String modelo6 = entradaPorTeclado.nextLine();
                    System.out.println("Posee camara Retro:");
                    boolean cam = true;
                    String opcion6 = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion6 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion6, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion6 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion6) {
                        case "1":
                            cam = true;
                            break;
                        case "2":
                            cam = false;
                            break;
                    }
                    System.out.println("¿Es convertible?:");
                    boolean conver = true;
                    String opcion9 = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion9 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion9, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion9 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion9) {
                        case "1":
                            conver = true;
                            break;
                        case "2":
                            conver = false;
                            break;
                    }
                    System.out.println("El motor es de gasolina:");
                    boolean gasolina6 = true;
                    String opcion8 = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion8 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion8, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion8 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion8) {
                        case "1":
                            gasolina6 = true;
                            break;
                        case "2":
                            gasolina6 = false;
                            break;
                    }
                    System.out.println("Ingrese el año del automovil:");
                    String año6 = entradaPorTeclado.nextLine();
                    while (!isCorrectYear(año6)) {
                        System.out.println("Ingrese un año correcto");
                        año6 = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Ingrese el precio del automovil");
                    String precio6 = entradaPorTeclado.nextLine();
                    while (!isNumeric(precio6)) {
                        System.out.println("Ingrese un valor correcto");
                        precio6 = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Ingrese el número de asientos");
                    String asientos = entradaPorTeclado.nextLine();
                    while (!isNumeric(asientos)) {
                        System.out.println("Ingrese un numero correcto");
                        asientos = entradaPorTeclado.nextLine();
                    }
                    double precio66 = Double.valueOf(precio6);
                    Automovil autonuevo = new Automovil(Integer.valueOf(asientos), conver, cam, marca6, modelo6, Integer.valueOf(año6), gasolina6, precio66);
                    System.out.println(Color.ANSI_PURPLE + "Se agrego con exito el automovil: \n");
                    System.out.println(Color.ANSI_PURPLE + autonuevo.toString());
                    SistemaActual.getListaVehiculos().add(autonuevo);

                    break;
                case "2":
                    System.out.println("Escriba la marca del Camion:");
                    String marca5 = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el modelo del Camion:");
                    String modelo5 = entradaPorTeclado.nextLine();
                    System.out.println("El motor es de gasolina:");
                    boolean gasolina5 = true;
                    String opcion5 = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion5 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion5, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion5 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion5) {
                        case "1":
                            gasolina5 = true;
                            break;
                        case "2":
                            gasolina5 = false;
                            break;
                    }
                    System.out.println("Ingrese el año del Camion:");
                    String año5 = entradaPorTeclado.nextLine();
                    while (!isCorrectYear(año5)) {
                        System.out.println("Ingrese un año correcto");
                        año5 = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Ingrese la capacidad del Camione en Kg:");
                    String capacidad = entradaPorTeclado.nextLine();
                    while (!isNumeric(capacidad)) {
                        System.out.println("Ingrese una capacidad correcta");
                        capacidad = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Ingrese el número de llantas del Camione en Kg:");
                    String llantas = entradaPorTeclado.nextLine();
                    while (!isNumeric(llantas)) {
                        System.out.println("Ingrese un número correcto(Numero Par:2,4,6,8...)");
                        llantas = entradaPorTeclado.nextLine();
                    }
                    if (!(Integer.valueOf(llantas) % 2 == 0)) {
                        while (!(Integer.valueOf(llantas) % 2 == 0)) {
                            System.out.println("Ingrese un número correcto(Numero Par:2,4,6,8...)");
                            llantas = entradaPorTeclado.nextLine();
                        }
                    }
                    System.out.println("Ingrese el precio del Camion:");
                    String precio5 = entradaPorTeclado.nextLine();
                    while (!isNumeric(precio5)) {
                        System.out.println("Ingrese un valor correcto");
                        precio5 = entradaPorTeclado.nextLine();
                    }
                    double precio55 = Double.valueOf(precio5);
                    Camion camionnuevo = new Camion(Integer.valueOf(capacidad), marca5, modelo5, Integer.valueOf(año5), gasolina5, Integer.valueOf(llantas), precio55);
                    System.out.println(Color.ANSI_PURPLE + "Se agrego con exito el camion: \n");
                    System.out.println(Color.ANSI_PURPLE + camionnuevo.toString());
                    SistemaActual.getListaVehiculos().add(camionnuevo);

                    break;
                case "3":
                    System.out.println("Eliga el tipo de motocicleta que desea agregar");
                    String tipoMoto = "";
                    String opcion2 = "";
                    System.out.println("Elija el tipo de motocicleta");
                    System.out.println("1. Deportiva.");
                    System.out.println("2. Scooter.");
                    System.out.println("3. Todo Terreno.");
                    opcion2 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion2, 3)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Deportiva.");
                        System.out.println("2. Scooter.");
                        System.out.println("3. Todo Terreno.");
                        opcion2 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion2) {
                        case "1":
                            tipoMoto = Motocicleta.Categorias[0];
                            break;
                        case "2":
                            tipoMoto = Motocicleta.Categorias[1];
                            break;
                        case "3":
                            tipoMoto = Motocicleta.Categorias[2];
                            break;
                    }
                    System.out.println("Escriba la marca de la moto:");
                    String marca = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el modelo de la moto:");
                    String modelo = entradaPorTeclado.nextLine();
                    System.out.println("El motor es de gasolina:");
                    boolean gasolina = true;
                    String opcion3 = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion3 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion3, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion3 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion3) {
                        case "1":
                            gasolina = true;
                            break;
                        case "2":
                            gasolina = false;
                            break;
                    }
                    System.out.println("Ingrese el año de la moto:");
                    String año = entradaPorTeclado.nextLine();
                    while (!isCorrectYear(año)) {
                        System.out.println("Ingrese un año correcto");
                        año = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Ingrese el precio de la moto:");
                    String precio = entradaPorTeclado.nextLine();
                    while (!isNumeric(precio)) {
                        System.out.println("Ingrese un valor correcto");
                        precio = entradaPorTeclado.nextLine();
                    }
                    double precio1 = Double.valueOf(precio);
                    Motocicleta motonueva = new Motocicleta(tipoMoto, marca, modelo, Integer.valueOf(año), gasolina, precio1);
                    System.out.println(Color.ANSI_PURPLE + "Se agrego con exito la Motocicleta: \n");
                    System.out.println(Color.ANSI_PURPLE + motonueva.toString());
                    SistemaActual.getListaVehiculos().add(motonueva);
                    break;
                case "4":
                    System.out.println("Escriba la marca del tractor:");
                    String marca4 = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el modelo del tractor:");
                    String modelo4 = entradaPorTeclado.nextLine();
                    System.out.println("Posee transmición hidraulica:");
                    boolean transm = true;
                    String opcion4 = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion4 = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion4, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion4 = entradaPorTeclado.nextLine();
                    }
                    switch (opcion4) {
                        case "1":
                            transm = true;
                            break;
                        case "2":
                            transm = false;
                            break;
                    }
                    System.out.println("¿Es para uso agricola?:");
                    boolean agricola = true;
                    String opcion = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    opcion = entradaPorTeclado.nextLine();
                    while (validarOpcion(opcion, 2)) {
                        System.out.println("Ingrese una opcion correcta.");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion = entradaPorTeclado.nextLine();
                    }
                    switch (opcion) {
                        case "1":
                            agricola = true;
                            break;
                        case "2":
                            agricola = false;
                            break;
                    }
                    System.out.println("Ingrese el año del tractor:");
                    String año4 = entradaPorTeclado.nextLine();
                    while (!isCorrectYear(año4)) {
                        System.out.println("Ingrese un año correcto");
                        año4 = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Ingrese el precio del tractor");
                    String precio4 = entradaPorTeclado.nextLine();
                    while (!isNumeric(precio4)) {
                        System.out.println("Ingrese un valor correcto");
                        precio4 = entradaPorTeclado.nextLine();
                    }
                    double precio44 = Double.valueOf(precio4);
                    Tractor tractornuevo = new Tractor(agricola, transm, marca4, modelo4, Integer.valueOf(año4), precio44);
                    System.out.println(Color.ANSI_PURPLE + "Se agrego con exito el tractor: \n");
                    System.out.println(Color.ANSI_PURPLE + tractornuevo.toString());
                    SistemaActual.getListaVehiculos().add(tractornuevo);

                    break;
                case "5":
                    break;
            }
        }
        ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
    }

    public void modificarVehiculo() {
        String opcion1 = "";
        System.out.println("Elija el vehiculo que desea modificar");
        int k = 0;
        for (Vehiculo v : SistemaActual.getListaVehiculos()) {
            k += 1;
            System.out.println(Color.ANSI_PURPLE + k + ".- " + v.toString());
        }
        opcion1 = entradaPorTeclado.nextLine();
        Vehiculo vget = null;
        if (Integer.valueOf(opcion1) < SistemaActual.getListaVehiculos().size()) {
            vget = SistemaActual.getListaVehiculos().get(Integer.valueOf(opcion1) - 1);
        }
        if (vget instanceof Automovil) {
            System.out.println("Escriba la marca del automovil:");
            String marca6 = entradaPorTeclado.nextLine();
            System.out.println("Escriba el modelo del automovil:");
            String modelo6 = entradaPorTeclado.nextLine();
            System.out.println("Posee camara Retro:");
            boolean cam = true;
            String opcion6 = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion6 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion6, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion6 = entradaPorTeclado.nextLine();
            }
            switch (opcion6) {
                case "1":
                    cam = true;
                    break;
                case "2":
                    cam = false;
                    break;
            }
            System.out.println("¿Es convertible?:");
            boolean conver = true;
            String opcion9 = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion9 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion9, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion9 = entradaPorTeclado.nextLine();
            }
            switch (opcion9) {
                case "1":
                    conver = true;
                    break;
                case "2":
                    conver = false;
                    break;
            }
            System.out.println("El motor es de gasolina:");
            boolean gasolina6 = true;
            String opcion8 = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion8 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion8, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion8 = entradaPorTeclado.nextLine();
            }
            switch (opcion8) {
                case "1":
                    gasolina6 = true;
                    break;
                case "2":
                    gasolina6 = false;
                    break;
            }
            System.out.println("Ingrese el año del automovil:");
            String año6 = entradaPorTeclado.nextLine();
            while (!isCorrectYear(año6)) {
                System.out.println("Ingrese un año correcto");
                año6 = entradaPorTeclado.nextLine();
            }
            System.out.println("Ingrese el precio del automovil");
            String precio6 = entradaPorTeclado.nextLine();
            while (!isNumeric(precio6)) {
                System.out.println("Ingrese un valor correcto");
                precio6 = entradaPorTeclado.nextLine();
            }
            System.out.println("Ingrese el número de asientos");
            String asientos = entradaPorTeclado.nextLine();
            while (!isNumeric(asientos)) {
                System.out.println("Ingrese un numero correcto");
                asientos = entradaPorTeclado.nextLine();
            }
            double precio66 = Double.valueOf(precio6);
            ((Automovil) vget).setNumeroAsientos(Integer.valueOf(asientos));
            ((Automovil) vget).setEsConvertible(conver);
            ((Automovil) vget).setTieneCamaraRetro(conver);
            vget.setMarca(marca6);
            vget.setModelo(modelo6);
            vget.setAñoFabricacion(Integer.valueOf(año6));
            vget.setMotorGasolina(gasolina6);
            vget.setPrecio(precio66);
            System.out.println(Color.ANSI_PURPLE + "Se actualizo con exito el automovil: \n");
            System.out.println(Color.ANSI_PURPLE + ((Automovil) vget).toString());

        } else if (vget instanceof Camion) {
            System.out.println("Escriba la marca del Camion:");
            String marca5 = entradaPorTeclado.nextLine();
            System.out.println("Escriba el modelo del Camion:");
            String modelo5 = entradaPorTeclado.nextLine();
            System.out.println("El motor es de gasolina:");
            boolean gasolina5 = true;
            String opcion5 = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion5 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion5, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion5 = entradaPorTeclado.nextLine();
            }
            switch (opcion5) {
                case "1":
                    gasolina5 = true;
                    break;
                case "2":
                    gasolina5 = false;
                    break;
            }
            System.out.println("Ingrese el año del Camion:");
            String año5 = entradaPorTeclado.nextLine();
            while (!isCorrectYear(año5)) {
                System.out.println("Ingrese un año correcto");
                año5 = entradaPorTeclado.nextLine();
            }
            System.out.println("Ingrese la capacidad del Camione en Kg:");
            String capacidad = entradaPorTeclado.nextLine();
            while (!isNumeric(capacidad)) {
                System.out.println("Ingrese una capacidad correcta");
                capacidad = entradaPorTeclado.nextLine();
            }
            System.out.println("Ingrese el número de llantas del Camione en Kg:");
            String llantas = entradaPorTeclado.nextLine();
            while (!isNumeric(llantas)) {
                System.out.println("Ingrese un número correcto(Numero Par:2,4,6,8...)");
                llantas = entradaPorTeclado.nextLine();
            }
            if (!(Integer.valueOf(llantas) % 2 == 0)) {
                while (!(Integer.valueOf(llantas) % 2 == 0)) {
                    System.out.println("Ingrese un número correcto(Numero Par:2,4,6,8...)");
                    llantas = entradaPorTeclado.nextLine();
                }
            }
            System.out.println("Ingrese el precio del Camion:");
            String precio5 = entradaPorTeclado.nextLine();
            while (!isNumeric(precio5)) {
                System.out.println("Ingrese un valor correcto");
                precio5 = entradaPorTeclado.nextLine();
            }
            double precio55 = Double.valueOf(precio5);
            vget.setAñoFabricacion(Integer.valueOf(año5));
            ((Camion) vget).setCapacidadCarga(Integer.valueOf(capacidad));
            vget.setAñoFabricacion(Integer.valueOf(año5));
            vget.setMarca(marca5);
            vget.setModelo(modelo5);
            vget.setMotorGasolina(gasolina5);
            vget.setNumeroLlantas(Integer.valueOf(llantas));
            vget.setPrecio(precio55);
            System.out.println(Color.ANSI_PURPLE + "Se actualizo con exito el camion: \n");
            System.out.println(Color.ANSI_PURPLE + ((Camion) vget).toString());

        } else if (vget instanceof Motocicleta) {
            System.out.println("Eliga el tipo de motocicleta que desea agregar");
            String tipoMoto = "";
            String opcion2 = "";
            System.out.println("Elija el tipo de motocicleta");
            System.out.println("1. Deportiva.");
            System.out.println("2. Scooter.");
            System.out.println("3. Todo Terreno.");
            opcion2 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion2, 3)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Deportiva.");
                System.out.println("2. Scooter.");
                System.out.println("3. Todo Terreno.");
                opcion2 = entradaPorTeclado.nextLine();
            }
            switch (opcion2) {
                case "1":
                    tipoMoto = Motocicleta.Categorias[0];
                    break;
                case "2":
                    tipoMoto = Motocicleta.Categorias[1];
                    break;
                case "3":
                    tipoMoto = Motocicleta.Categorias[2];
                    break;
            }
            System.out.println("Escriba la marca de la moto:");
            String marca = entradaPorTeclado.nextLine();
            System.out.println("Escriba el modelo de la moto:");
            String modelo = entradaPorTeclado.nextLine();
            System.out.println("El motor es de gasolina:");
            boolean gasolina = true;
            String opcion3 = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion3 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion3, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion3 = entradaPorTeclado.nextLine();
            }
            switch (opcion3) {
                case "1":
                    gasolina = true;
                    break;
                case "2":
                    gasolina = false;
                    break;
            }
            System.out.println("Ingrese el año de la moto:");
            String año = entradaPorTeclado.nextLine();
            while (!isCorrectYear(año)) {
                System.out.println("Ingrese un año correcto");
                año = entradaPorTeclado.nextLine();
            }
            System.out.println("Ingrese el precio de la moto:");
            String precio = entradaPorTeclado.nextLine();
            while (!isNumeric(precio)) {
                System.out.println("Ingrese un valor correcto");
                precio = entradaPorTeclado.nextLine();
            }
            double precio1 = Double.valueOf(precio);
            ((Motocicleta) vget).setCategoria(tipoMoto);
            vget.setMarca(marca);
            vget.setModelo(modelo);
            vget.setAñoFabricacion(Integer.valueOf(año));
            vget.setMotorGasolina(gasolina);
            vget.setPrecio(precio1);
            System.out.println(Color.ANSI_PURPLE + "Se actualizo con exito la Motocicleta: \n");
            System.out.println(Color.ANSI_PURPLE + ((Motocicleta) vget).toString());
        } else if (vget instanceof Tractor) {
            System.out.println("Escriba la marca del tractor:");
            String marca4 = entradaPorTeclado.nextLine();
            System.out.println("Escriba el modelo del tractor:");
            String modelo4 = entradaPorTeclado.nextLine();
            System.out.println("Posee transmición hidraulica:");
            boolean transm = true;
            String opcion4 = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion4 = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion4, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion4 = entradaPorTeclado.nextLine();
            }
            switch (opcion4) {
                case "1":
                    transm = true;
                    break;
                case "2":
                    transm = false;
                    break;
            }
            System.out.println("¿Es para uso agricola?:");
            boolean agricola = true;
            String opcion = "";
            System.out.println("1. Si.");
            System.out.println("2. No.");
            opcion = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion, 2)) {
                System.out.println("Ingrese una opcion correcta.");
                System.out.println("1. Si.");
                System.out.println("2. No.");
                opcion = entradaPorTeclado.nextLine();
            }
            switch (opcion) {
                case "1":
                    agricola = true;
                    break;
                case "2":
                    agricola = false;
                    break;
            }
            System.out.println("Ingrese el año del tractor:");
            String año4 = entradaPorTeclado.nextLine();
            while (!isCorrectYear(año4)) {
                System.out.println("Ingrese un año correcto");
                año4 = entradaPorTeclado.nextLine();
            }
            System.out.println("Ingrese el precio del tractor");
            String precio4 = entradaPorTeclado.nextLine();
            while (!isNumeric(precio4)) {
                System.out.println("Ingrese un valor correcto");
                precio4 = entradaPorTeclado.nextLine();
            }
            double precio44 = Double.valueOf(precio4);
            vget.setAñoFabricacion(Integer.valueOf(año4));
            ((Tractor) vget).setTransimisionHidraulica(transm);
            ((Tractor) vget).setUsoAgricola(agricola);
            vget.setMarca(marca4);
            vget.setModelo(modelo4);
            vget.setPrecio(precio44);
            System.out.println(Color.ANSI_PURPLE + "Se actualizo con exito el tractor: \n");
            System.out.println(Color.ANSI_PURPLE + ((Tractor) vget).toString());
        } else {
            System.out.println(Color.ANSI_RED + "No se encontro vehiculo en dicha posición pruebe nuevamente con un número dentro del rango"
                    + " mostrado en pantalla");
        }
        ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
    }

    /**
     * Modifica los detalles de los empleados de la concesionaria.
     */
    public void modificarEmpleado(ArrayList<Usuario> Usuarios, Supervisor S) {
        verEmpleadosDelSistema();
        System.out.println("Ingrese el Id del empleado que desea modificar:");
        String userid = entradaPorTeclado.nextLine();
        while (!isNumeric(userid)) {
            System.out.println("Ingrese un Id Correcto");
            userid = entradaPorTeclado.nextLine();
        }
        int count = 0;
        for (Usuario user : Usuarios) {
            if ((user instanceof Empleado)) {
                Empleado castEmpl = (Empleado) user;
                if (castEmpl.getId() == Integer.valueOf(userid)) {
                    count += 1;
                    System.out.println("Escriba los nuevos nombres:");
                    String nombres = entradaPorTeclado.nextLine();
                    System.out.println("Escriba los nuevos apellidos:");
                    String apellidos = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el nuevo usuario:");
                    String usern = entradaPorTeclado.nextLine();
                    System.out.println("Escriba la nueva contraseña:");
                    System.out.println("SOLO DEBE SER NUMERICA Y DEBE TENER DE 5 a 12 digitos");
                    String pwdn = entradaPorTeclado.nextLine();
                    SistemaActual.buscarEmpleadoId(Integer.valueOf(userid)).setNombres(nombres);
                    SistemaActual.buscarEmpleadoId(Integer.valueOf(userid)).setApellidos(apellidos);
                    SistemaActual.buscarEmpleadoId(Integer.valueOf(userid)).setUser(usern);
                    SistemaActual.buscarEmpleadoId(Integer.valueOf(userid)).setContrasena(pwdn);
                    if (castEmpl instanceof JefeTaller) {
                        String certificaciones = "";
                        System.out.println("¿Desea agregar otra certificacion tecnica?:");
                        String opcion9 = "";
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion9 = entradaPorTeclado.nextLine();
                        while (validarOpcion(opcion9, 2)) {
                            System.out.println("Ingrese una opcion correcta.");
                            System.out.println("1. Si.");
                            System.out.println("2. No.");
                            opcion9 = entradaPorTeclado.nextLine();
                        }
                        switch (opcion9) {
                            case "1":
                                System.out.println("Ingrese las certificaciones separada por una coma");
                                certificaciones = entradaPorTeclado.nextLine();
                                if (!certificaciones.equals("")) {
                                    String ListaC[] = certificaciones.split("");
                                    List<String> ctf = Arrays.asList(ListaC);
                                    for (String c : ctf) {
                                        ((JefeTaller) castEmpl).getCertificacionesTecnicas().add(c);
                                    }
                                }
                                break;
                            case "2":
                                break;
                        }
                    } else if (castEmpl instanceof Supervisor) {
                        String certificaciones = "";
                        System.out.println("¿Desea agregar otra certificacion Academicas?:");
                        String opcion9 = ",";
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        opcion9 = entradaPorTeclado.nextLine();
                        while (validarOpcion(opcion9, 2)) {
                            System.out.println("Ingrese una opcion correcta.");
                            System.out.println("1. Si.");
                            System.out.println("2. No.");
                            opcion9 = entradaPorTeclado.nextLine();
                        }
                        switch (opcion9) {
                            case "1":
                                System.out.println("Ingrese las certificaciones separada por una coma");
                                certificaciones = entradaPorTeclado.nextLine();
                                if (!certificaciones.equals("")) {
                                    String ListaC[] = certificaciones.split(",");
                                    List<String> ctf = Arrays.asList(ListaC);
                                    for (String c : ctf) {
                                        ((Supervisor) castEmpl).getCertificacionesAcademicas().add(c);
                                    }
                                }
                                break;
                            case "2":
                                break;
                        }
                    }
                }
            }
        }
        if (count == 0) {
            System.out.println(Color.ANSI_RED + "NO EXISTE UN EMPLEADO CON EL ID INGRESADO");
        }
        ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
    }

    public void MenuVendedor(Vendedor vendedor) throws IOException {
        String opcion = "";
        while (!"8".equals(opcion)) {
            System.out.println("  __  __                   __      __            _          _                     \n"
                    + " |  \\/  |                  \\ \\    / /           | |        | |                    \n"
                    + " | \\  / | ___ _ __  _   _   \\ \\  / /__ _ __   __| | ___  __| | ___  _ __ ___  ___ \n"
                    + " | |\\/| |/ _ \\ '_ \\| | | |   \\ \\/ / _ \\ '_ \\ / _` |/ _ \\/ _` |/ _ \\| '__/ _ \\/ __|\n"
                    + " | |  | |  __/ | | | |_| |    \\  /  __/ | | | (_| |  __/ (_| | (_) | | |  __/\\__ \\\n"
                    + " |_|  |_|\\___|_| |_|\\__,_|     \\/ \\___|_| |_|\\__,_|\\___|\\__,_|\\___/|_|  \\___||___/\n"
                    + "                                                                                  \n"
                    + "                                                                                  ");
            System.out.println("1. Ingresar nuevos vehículos al stock de la concesionaria.");
            System.out.println("2. Modificar los detalles de los vehículos disponibles para venta.");
            System.out.println("3. Consultar los vehículos que se encuentran en reparación.");
            System.out.println("4. Consultar vehículos que ha vendido.");
            System.out.println("5. Ver el valor de mi bono.");
            System.out.println("6. Revisar Cotizaciones.");
            System.out.println("7. Revisar mis Ventas.");
            System.out.println("8. Salir");
            opcion = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion, 8)) {
                System.out.println(Color.ANSI_RED + "Ingrese una opcion correcta.");
                System.out.println("1. Ingresar nuevos vehículos al stock de la concesionaria.");
                System.out.println("2. Modificar los detalles de los vehículos disponibles para venta.");
                System.out.println("3. Consultar los vehículos que se encuentran en reparación.");
                System.out.println("4. Consultar vehículos que ha vendido.");
                System.out.println("5. Ver el valor de mi bono.");
                System.out.println("6. Revisar Cotizaciones.");
                System.out.println("7. Revisar mis Ventas.");
                System.out.println("8. Salir");
                opcion = entradaPorTeclado.nextLine();
            }
            switch (opcion) {
                case "1":
                    agregarVehiculo();
                    break;
                case "2":
                    modificarVehiculo();
                    break;
                case "3":
                    SistemaActual.verMantenimientos();
                    break;
                case "4":
                    for (Vehiculo v : vendedor.getVentas()) {
                        System.out.println(Color.ANSI_PURPLE + v.toString());
                    }
                    break;
                case "5":
                    System.out.println(Color.ANSI_CYAN + "Su bono de venta es de: " + vendedor.getBonoVenta() + " por " + vendedor.getVentas().size() + " vehículos vendidos.");
                    break;
                case "6":
                    System.out.println("Elija las cotizaciones que desea revisar: ");
                    int b = 0;
                    String revisarcotizacion = "";
                    for (Solicitud s : vendedor.getCotizaciones()) {
                        b += 1;
                        System.out.println(Color.ANSI_PURPLE + b + ".- " + s.toStringCliente());
                    }
                    revisarcotizacion = entradaPorTeclado.nextLine();
                    while (!(Integer.valueOf(revisarcotizacion) > 0 && Integer.valueOf(revisarcotizacion) <= vendedor.getCotizaciones().size())) {
                        System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                        System.out.println("Intente nuevamente...");
                        revisarcotizacion = entradaPorTeclado.nextLine();
                    }
                    System.out.println("¿Dese aceptar esta cotizacion?:");
                    String aceptacioncoti = "";
                    String motivo = "";
                    System.out.println("1. Si.");
                    System.out.println("2. No.");
                    System.out.println("3. Salir.");
                    aceptacioncoti = entradaPorTeclado.nextLine();
                    while (validarOpcion(aceptacioncoti, 3)) {
                        System.out.println(Color.ANSI_RED + "Elija una opción correcta!");
                        System.out.println("1. Si.");
                        System.out.println("2. No.");
                        System.out.println("3. Salir.");
                        aceptacioncoti = entradaPorTeclado.nextLine();
                    }
                    switch (aceptacioncoti) {
                        case "1":
                            SistemaActual.cambioEstadoSolicitud(vendedor.getCotizaciones().get(Integer.valueOf(revisarcotizacion) - 1), true, motivo);
                            vendedor.getCotizaciones().get(Integer.valueOf(revisarcotizacion) - 1).setEstaAprobada(true);
                            break;
                        case "2":
                            System.out.println("Ingrese el motivo del rechazo: ");
                            motivo = entradaPorTeclado.nextLine();
                            SistemaActual.cambioEstadoSolicitud(vendedor.getCotizaciones().get(Integer.valueOf(revisarcotizacion) - 1), false, motivo);
                            vendedor.getCotizaciones().get(Integer.valueOf(revisarcotizacion) - 1).setEstaAprobada(false);
                            vendedor.getCotizaciones().get(Integer.valueOf(revisarcotizacion) - 1).setMotivoRechazo(motivo);
                            break;
                        case "3":
                            break;
                    }
                    ManejoDeArchivos.guardarSolicitudes(SistemaActual.getListaSolicitudes());
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
                    break;
                case "7":
                    int b2 = 0;
                    for (Solicitud s : vendedor.getCotizaciones()) {
                        if ((s instanceof Compra) && (s.isEstaAprobada() == true)) {
                            b2 += 1;
                            System.out.println(Color.ANSI_PURPLE + b2 + ".- " + s.toStringCliente());
                        }
                    }
                    break;
                case "8":
                    break;
            }
        }
    }

    public void MenuJefeTaller(JefeTaller jefeTaller) throws IOException {
        String opcion = "";
        while (!"3".equals(opcion)) {
            System.out.println("  __  __                         _       __           _        _______    _ _           \n"
                    + " |  \\/  |                       | |     / _|         | |      |__   __|  | | |          \n"
                    + " | \\  / | ___ _ __  _   _       | | ___| |_ ___    __| | ___     | | __ _| | | ___ _ __ \n"
                    + " | |\\/| |/ _ \\ '_ \\| | | |  _   | |/ _ \\  _/ _ \\  / _` |/ _ \\    | |/ _` | | |/ _ \\ '__|\n"
                    + " | |  | |  __/ | | | |_| | | |__| |  __/ ||  __/ | (_| |  __/    | | (_| | | |  __/ |   \n"
                    + " |_|  |_|\\___|_| |_|\\__,_|  \\____/ \\___|_| \\___|  \\__,_|\\___|    |_|\\__,_|_|_|\\___|_|   \n"
                    + "                                                                                        \n"
                    + "                                                                                       ");
            System.out.println("1. Registrar partes y repuestos utilizados en un mantenimiento.");
            System.out.println("2. Ver y cambiar estados de un mantenimientos.");
            System.out.println("3. Salir");;
            opcion = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion, 3)) {
                System.out.println(Color.ANSI_RED + "Ingrese una opcion correcta.");
                System.out.println("1. Registrar partes y repuestos utilizados en un mantenimiento.");
                System.out.println("2. Ver y cambiar estados de un mantenimientos.");
                System.out.println("3. Salir");
                opcion = entradaPorTeclado.nextLine();
            }
            switch (opcion) {
                case "1":
                    SistemaActual.verMantenimientos();
                    System.out.println("Ingrese el Id del mantenimiento al que desea registrar repuestos:");
                    String mid = entradaPorTeclado.nextLine();
                    String rep = "";
                    while (!isNumeric(mid)) {
                        System.out.println("Ingrese un Id Correcto");
                        mid = entradaPorTeclado.nextLine();
                    }
                    Mantenimiento mmod = SistemaActual.buscarMantenimientoId(Integer.valueOf(mid));
                    if (!(mmod == null)) {
                        System.out.println("Ingrese los repuestos separados por una coma");
                        rep = entradaPorTeclado.nextLine();
                        if (!rep.equals("")) {
                            String ListaR[] = rep.split(",");
                            List<String> rtf = Arrays.asList(ListaR);
                            for (String p : rtf) {
                                SistemaActual.buscarMantenimientoId(Integer.valueOf(mid)).anadirPieza(p);
                            }
                        }
                    } else {
                        System.out.println(Color.ANSI_RED + "NO EXISTE UN MANTENIMIENTO CON EL ID INGRESADO");
                    }
                    ManejoDeArchivos.guardarMantenimientos(SistemaActual.getListaMantenimientos());
                    ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
                    break;
                case "2":
                    System.out.println("Elija el mantenimiento que desea cambiar de estado: ");
                    String posman = "";
                    int cm = 0;
                    for (Mantenimiento m : SistemaActual.getListaMantenimientos()) {
                        cm += 1;
                        System.out.println(Color.ANSI_PURPLE + cm + ".- " + m.toString());
                    }
                    posman = entradaPorTeclado.nextLine();
                    while (!(Integer.valueOf(posman) > 0 && Integer.valueOf(posman) <= SistemaActual.getListaMantenimientos().size())) {
                        System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                        System.out.println("Intente nuevamente...");
                        posman = entradaPorTeclado.nextLine();
                    }
                    System.out.println("Elija el estado al que desea mover:");
                    System.out.println(Color.ANSI_BLUE + "Recuerde que los que se encuentra sin revision es porque no han sido aceptados.");
                    String est = "";
                    System.out.println("1. Admitido.");
                    System.out.println("2. En reparación.");
                    System.out.println("3. En prueba.");
                    System.out.println("4. Devuelto.");
                    System.out.println("5. Salir.");
                    est = entradaPorTeclado.nextLine();
                    while (validarOpcion(est, 5)) {
                        System.out.println(Color.ANSI_RED + "Elija una opción correcta:");
                        System.out.println(Color.ANSI_BLUE + "Recuerde que los que se encuentra sin revision es porque no han sido aceptados.");
                        System.out.println("1. Admitido.");
                        System.out.println("2. En reparación.");
                        System.out.println("3. En prueba.");
                        System.out.println("4. Devuelto.");
                        System.out.println("5. Salir.");
                        est = entradaPorTeclado.nextLine();
                    }

                    switch (est) {
                        case "1":
                            if ((SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).getEstado().equals("Sin Revision")) && (SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).isEsEmergente() == true)) {
                                String preciomant = "";
                                System.out.println("Ingrese el precio del mantenimiento emergente: ");
                                preciomant = entradaPorTeclado.nextLine();
                                while (!isNumeric(preciomant)) {
                                    System.out.println("Ingrese un valor correcto");
                                    preciomant = entradaPorTeclado.nextLine();
                                }
                                SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setPrecio(Double.valueOf(preciomant));
                            }
                            SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setEstado(Mantenimiento.Estados[0]);
                            break;
                        case "2":
                            if ((SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).getEstado().equals("Sin Revision")) && (SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).isEsEmergente() == true)) {
                                String preciomant = "";
                                System.out.println("Ingrese el precio del mantenimiento emergente: ");
                                preciomant = entradaPorTeclado.nextLine();
                                while (!isNumeric(preciomant)) {
                                    System.out.println("Ingrese un valor correcto");
                                    preciomant = entradaPorTeclado.nextLine();
                                }
                                SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setPrecio(Double.valueOf(preciomant));
                            }
                            SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setEstado(Mantenimiento.Estados[1]);
                            break;
                        case "3":
                            if ((SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).getEstado().equals("Sin Revision")) && (SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).isEsEmergente() == true)) {
                                String preciomant = "";
                                System.out.println("Ingrese el precio del mantenimiento emergente: ");
                                preciomant = entradaPorTeclado.nextLine();
                                while (!isNumeric(preciomant)) {
                                    System.out.println("Ingrese un valor correcto");
                                    preciomant = entradaPorTeclado.nextLine();
                                }
                                SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setPrecio(Double.valueOf(preciomant));
                            }
                            SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setEstado(Mantenimiento.Estados[2]);
                            break;
                        case "4":
                            if ((SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).getEstado().equals("Sin Revision")) && (SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).isEsEmergente() == true)) {
                                String preciomant = "";
                                System.out.println("Ingrese el precio del mantenimiento emergente: ");
                                preciomant = entradaPorTeclado.nextLine();
                                while (!isNumeric(preciomant)) {
                                    System.out.println("Ingrese un valor correcto");
                                    preciomant = entradaPorTeclado.nextLine();
                                }
                                SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setPrecio(Double.valueOf(preciomant));
                            }
                            SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1).setEstado(Mantenimiento.Estados[3]);
                            SistemaActual.getListaMantenimientos().remove(SistemaActual.getListaMantenimientos().get(Integer.valueOf(posman) - 1));
                            break;
                        case "5":
                            break;
                    }
                    ManejoDeArchivos.guardarMantenimientos(SistemaActual.getListaMantenimientos());
                    ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    break;
                case "3":
                    break;

            }
        }

    }

    public void MenuCliente(Cliente cliente) throws IOException {
        String opcion = "";
        while (!"11".equals(opcion)) {
            System.out.println("  __  __                     _____ _ _            _       \n"
                    + " |  \\/  |                   / ____| (_)          | |      \n"
                    + " | \\  / | ___ _ __  _   _  | |    | |_  ___ _ __ | |_ ___ \n"
                    + " | |\\/| |/ _ \\ '_ \\| | | | | |    | | |/ _ \\ '_ \\| __/ _ \\\n"
                    + " | |  | |  __/ | | | |_| | | |____| | |  __/ | | | ||  __/\n"
                    + " |_|  |_|\\___|_| |_|\\__,_|  \\_____|_|_|\\___|_| |_|\\__\\___|\n"
                    + "                                                          \n"
                    + "                                                         ");
            System.out.println("1. Ver mi información personal.");
            System.out.println("2. Modificar información personal.");
            System.out.println("3. Consultar Stock.");
            System.out.println("4. Agregar a mi WishList.");
            System.out.println("5. Ver a mi WishList.");
            System.out.println("6. Ver mis cotizaciones.");
            System.out.println("7. Ver mis solicitudes de compra.");
            System.out.println("8. Hacer una nueva solicitud.");
            System.out.println("9. Ver estado de mi vehículos que están en mantenimiento.");
            System.out.println("10 Solicitar un nuevo mantenimiento.");
            System.out.println("11. Salir");
            opcion = entradaPorTeclado.nextLine();
            while (validarOpcion(opcion, 11)) {
                System.out.println(Color.ANSI_RED + "Ingrese una opcion correcta.");
                System.out.println("1. Ver información personal.");
                System.out.println("2. Modificar información personal.");
                System.out.println("3. Consultar Stock.");
                System.out.println("4. Agregar a mi WishList.");
                System.out.println("5. Ver a mi WishList.");
                System.out.println("6. Ver mis cotizaciones.");
                System.out.println("7. Ver mis solicitudes de compra.");
                System.out.println("8. Hacer una nueva solicitud.");
                System.out.println("9. Ver estado de mi vehículos que están en mantenimiento.");
                System.out.println("10 Solicitar un nuevo mantenimiento.");
                System.out.println("11. Salir.");
                opcion = entradaPorTeclado.nextLine();
            }
            switch (opcion) {
                case "1":
                    System.out.println(Color.ANSI_PURPLE + cliente.toString());
                    break;
                case "2":
                    System.out.println("Escriba los nuevos nombres:");
                    String nombres = entradaPorTeclado.nextLine();
                    System.out.println("Escriba los nuevos apellidos:");
                    String apellidos = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el nuevo usuario:");
                    String usern = entradaPorTeclado.nextLine();
                    System.out.println("Escriba la nueva contraseña:");
                    System.out.println("SOLO DEBE SER NUMERICA Y DEBE TENER DE 5 a 12 digitos");
                    String pwdn = entradaPorTeclado.nextLine();
                    System.out.println("Escriba el nuevo empleo:");
                    String empleo = entradaPorTeclado.nextLine();
                    System.out.println("Escriba la nueva cedula:");
                    String cedula = entradaPorTeclado.nextLine();
                    System.out.println("Ingrese el nuevo sueldo:");
                    String sueldo = entradaPorTeclado.nextLine();
                    while (!isNumeric(sueldo)) {
                        System.out.println("Ingrese un valor correcto");
                        sueldo = entradaPorTeclado.nextLine();
                    }
                    cliente.modificarInformacion(cedula, empleo, Double.valueOf(sueldo), usern, pwdn, nombres, apellidos);
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    break;
                case "3":
                    String opcionc = "";
                    while (!"3".equals(opcionc)) {
                        System.out.println("Elija el tipo de consulta que desea realizar:");
                        System.out.println("1. Consulta por tipo de Vehículo.");
                        System.out.println("2. Consulta por marca.");
                        System.out.println("3. Salir.");
                        opcionc = entradaPorTeclado.nextLine();
                        while (validarOpcion(opcionc, 3)) {
                            System.out.println("Ingrese una opcion correcta.");
                            System.out.println("1. Consulta por tipo de Vehículo.");
                            System.out.println("2. Consulta por marca.");
                            System.out.println("3. Salir.");
                            opcionc = entradaPorTeclado.nextLine();
                        }
                        switch (opcionc) {
                            case "1":
                                String opciontp = "";
                                while (!"5".equals(opciontp)) {
                                    System.out.println("Elija el tipo de vehiculo que desea visualizar:");
                                    System.out.println("1. Automovil.");
                                    System.out.println("2. Camion.");
                                    System.out.println("3. Motocicleta.");
                                    System.out.println("4. Tractor.");
                                    System.out.println("5. Salir.");
                                    opciontp = entradaPorTeclado.nextLine();
                                    while (validarOpcion(opciontp, 5)) {
                                        System.out.println(Color.ANSI_RED + "Elija el tipo de vehiculo correcto!!!");
                                        System.out.println("1. Automovil.");
                                        System.out.println("2. Camion.");
                                        System.out.println("3. Motocicleta.");
                                        System.out.println("4. Tractor.");
                                        System.out.println("5. Salir");
                                        opciontp = entradaPorTeclado.nextLine();
                                    }
                                    switch (opciontp) {
                                        case "1":
                                            System.out.println(Color.ANSI_GREEN + "Automoviles");
                                            SistemaActual.verAutomoviles();
                                            break;
                                        case "2":
                                            System.out.println(Color.ANSI_GREEN + "Camiones");
                                            SistemaActual.verCamiones();
                                            break;
                                        case "3":
                                            System.out.println(Color.ANSI_GREEN + "Motocicletas");
                                            SistemaActual.verMotos();
                                            break;
                                        case "4":
                                            System.out.println(Color.ANSI_GREEN + "Tractores");
                                            SistemaActual.verTractores();
                                            break;
                                        case "5":
                                            break;
                                    }
                                }
                                break;
                            case "2":
                                System.out.println("Ingrese la marca del vehiculo que desea visualizar:");
                                String opcionv = "";
                                opcionv = entradaPorTeclado.nextLine();
                                int bandera = 0;
                                for (Vehiculo v : SistemaActual.getListaVehiculos()) {
                                    if (v.getMarca().toLowerCase().equals(opcionv.toLowerCase()) && (v.getVendedor() == null)) {
                                        System.out.println(Color.ANSI_PURPLE + v.visorClientes());
                                        bandera += 1;
                                    }
                                }
                                if (bandera == 0) {
                                    System.out.println(Color.ANSI_RED + "No existe vehículo con la marca: " + opcionv);
                                }
                                break;
                            case "3":
                                break;
                        }
                    }
                    break;
                case "4":
                    System.out.println("Elija el número de vehiculo que desea agregar"
                            + " a su Lista de Deseos");
                    int k = 0;
                    int k1 = 0;
                    for (Vehiculo v : SistemaActual.getListaVehiculos()) {
                        k += 1;
                        k1 += 1;
                        if (v.isEstaSolicitado() == false) {
                            System.out.println(Color.ANSI_PURPLE + k1 + ".- " + v.visorClientes());
                        }
                    }
                    String deseo = entradaPorTeclado.nextLine();
                    if (Integer.valueOf(deseo) > 0 && Integer.valueOf(deseo) <= SistemaActual.vehiculosDisponibles()) {
                        cliente.getWishList().add(SistemaActual.getListaVehiculos().get((Integer.valueOf(deseo)) - 1));
                        System.out.println(Color.ANSI_GREEN + "SE AGREGO EL VEHICULO CON EXITO!");
                    } else {
                        System.out.println(Color.ANSI_RED + "No existe el numero de vehiculo solicitado \n");
                    }
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    break;
                case "5":
                    int val = 0;
                    for (Vehiculo v : cliente.getWishList()) {
                        System.out.println(Color.ANSI_PURPLE + v.visorClientes());
                        val += 1;
                    }
                    if (val == 0) {
                        System.out.println(Color.ANSI_RED + "No existe vehiculos agregados en su WhishList!");
                    }
                    break;
                case "6":
                    int countercotizar = 0;
                    for (Solicitud s : SistemaActual.getListaSolicitudes()) {
                        if ((s.getCliente().equals(cliente)) && (s instanceof Cotizacion)) {
                            countercotizar += 1;
                            System.out.println(Color.ANSI_GREEN + ((Cotizacion) s).toStringCliente());
                        }
                    }
                    if (countercotizar == 0) {
                        System.out.println(Color.ANSI_RED + "No tiene cotizaciones enviadas.");
                    }
                    break;
                case "7":
                    int countercompras = 0;
                    for (Solicitud s : SistemaActual.getListaSolicitudes()) {
                        if ((s.getCliente().equals(cliente)) && (s instanceof Compra)) {
                            countercompras += 1;
                            System.out.println(Color.ANSI_GREEN + ((Compra) s).toStringCliente());
                        }
                    }
                    if (countercompras == 0) {
                        System.out.println(Color.ANSI_RED + "No tiene solicitudes de compra enviadas.");
                    }
                    break;
                case "8":
                    System.out.println("Elija el tipo de solicitud que desea realizar:");
                    String sol = "";
                    System.out.println("1. Cotización de vehículo.");
                    System.out.println("2. Solicitud de compra.");
                    System.out.println("3. Salir.");
                    sol = entradaPorTeclado.nextLine();
                    while (validarOpcion(sol, 3)) {
                        System.out.println(Color.ANSI_RED + "Elija un tipo de solicitud correcta!");
                        System.out.println("1. Cotización de vehículo.");
                        System.out.println("2. Solicitud de compra.");
                        System.out.println("3. Salir.");
                        sol = entradaPorTeclado.nextLine();
                    }
                    switch (sol) {
                        case "1":
                            String posvehi = "";
                            System.out.println(Color.ANSI_GREEN + "Elija el Vehiculo que desea cotizar: ");
                            int j = 0;
                            int j3 = 0;
                            for (Vehiculo v : SistemaActual.getListaVehiculos()) {
                                j += 1;
                                if (v.isEstaSolicitado() == false) {
                                    j3 += 1;
                                    System.out.println(Color.ANSI_PURPLE + j3 + ".- " + v.visorClientes());
                                }
                            }
                            posvehi = entradaPorTeclado.nextLine();
                            while (!(Integer.valueOf(posvehi) > 0 && Integer.valueOf(posvehi) <= SistemaActual.vehiculosDisponibles())) {
                                System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                                System.out.println("Intente nuevamente...");
                                posvehi = entradaPorTeclado.nextLine();
                            }
                            System.out.println("Elija el vendedor por el cual fue atendido: ");
                            String posvend = "";
                            int l = 0;
                            for (Vendedor v : SistemaActual.getListaVendedores()) {
                                l += 1;
                                System.out.println(Color.ANSI_PURPLE + l + ".- " + v.toStringCliente());
                            }
                            posvend = entradaPorTeclado.nextLine();
                            while (!(Integer.valueOf(posvend) > 0 && Integer.valueOf(posvend) <= SistemaActual.getListaVendedores().size())) {
                                System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                                System.out.println("Intente nuevamente...");
                                posvend = entradaPorTeclado.nextLine();
                            }
                            Cotizacion nuevacot = new Cotizacion(cliente, SistemaActual.getListaVendedores().get(Integer.valueOf(posvend) - 1), SistemaActual.getListaVehiculos().get(Integer.valueOf(posvehi) - 1));
                            SistemaActual.getListaVendedores().get(Integer.valueOf(posvend) - 1).anadirCotizacion(nuevacot);
                            SistemaActual.getListaSolicitudes().add(nuevacot);
                            break;
                        case "2":
                            String posvehi2 = "";
                            System.out.println(Color.ANSI_GREEN + "Elija el Vehiculo que desea cotizar: ");
                            int j2 = 0;
                            int j4 = 0;
                            for (Vehiculo v : SistemaActual.getListaVehiculos()) {
                                j2 += 1;
                                if (v.isEstaSolicitado() == false) {
                                    j4 += 1;
                                    System.out.println(Color.ANSI_PURPLE + j4 + ".- " + v.visorClientes());
                                }
                            }
                            posvehi2 = entradaPorTeclado.nextLine();
                            while (!(Integer.valueOf(posvehi2) > 0 && Integer.valueOf(posvehi2) <= SistemaActual.vehiculosDisponibles())) {
                                System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                                System.out.println("Intente nuevamente...");
                                posvehi2 = entradaPorTeclado.nextLine();
                            }
                            System.out.println("Elija el vendedor por el cual fue atendido: ");
                            String posvend2 = "";
                            int l2 = 0;
                            for (Vendedor v : SistemaActual.getListaVendedores()) {
                                l2 += 1;
                                System.out.println(Color.ANSI_PURPLE + l2 + ".- " + v.toStringCliente());
                            }
                            posvend2 = entradaPorTeclado.nextLine();
                            while (!(Integer.valueOf(posvend2) > 0 && Integer.valueOf(posvend2) <= SistemaActual.getListaVendedores().size())) {
                                System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                                System.out.println("Intente nuevamente...");
                                posvend2 = entradaPorTeclado.nextLine();
                            }
                            int numeroaleatorio = (int) (Math.random() * SistemaActual.getListaSupervisores().size());

                            Compra nuevacomp = new Compra(SistemaActual.getListaSupervisores().get(numeroaleatorio), cliente, SistemaActual.getListaVendedores().get(Integer.valueOf(posvend2) - 1), SistemaActual.getListaVehiculos().get(Integer.valueOf(posvehi2) - 1));
                            SistemaActual.getListaVehiculos().get(Integer.valueOf(posvehi2) - 1).setEstaSolicitado(true);
                            SistemaActual.getListaSupervisores().get(numeroaleatorio).anadirSolicitudCompra(nuevacomp);
                            SistemaActual.getListaSolicitudes().add(nuevacomp);
                            break;
                        case "3":
                            break;
                    }
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    ManejoDeArchivos.guardarSolicitudes(SistemaActual.getListaSolicitudes());
                    ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
                    break;
                case "9":
                    int mant = 0;
                    for (Mantenimiento m : SistemaActual.getListaMantenimientos()) {
                        if (m.getCliente().equals(cliente)) {
                            mant += 1;
                            System.out.println(m.toString());
                        }
                    }
                    if (mant == 0) {
                        System.out.println("No tiene vehiculos en mantenimiento.");
                    }
                    break;
                case "10":
                    System.out.println("Elija el tipo de mantenimiento deseado: ");
                    String mante = "";
                    System.out.println("1. Mantenimiento Preventivo.");
                    System.out.println("2. Mantenimiento Emergente.");
                    System.out.println("3. Salir.");
                    mante = entradaPorTeclado.nextLine();
                    while (validarOpcion(mante, 3)) {
                        System.out.println(Color.ANSI_RED + "Elija un tipo de solicitud correcta!");
                        System.out.println("1. Mantenimiento Preventivo.");
                        System.out.println("2. Mantenimiento Emergente.");
                        System.out.println("3. Salir.");
                        mante = entradaPorTeclado.nextLine();
                    }
                    switch (mante) {
                        case "1":
                            String numv = "";
                            int countvc = 0;
                            if (cliente.getVehiculosComprados().size() == 0) {
                                System.err.println(Color.ANSI_RED + "No ha comprado vehiculos para solicitar un mantenimiento.\n");
                            } else {
                                System.out.println("Elija el vehículo que desea enviar a mantenimiento preventivo: ");
                                for (Vehiculo v : cliente.getVehiculosComprados()) {
                                    countvc += 1;
                                    System.out.println(Color.ANSI_BLUE + countvc + ".- " + v.visorClientesV());
                                }
                                numv = entradaPorTeclado.nextLine();
                                while (!(Integer.valueOf(numv) > 0 && Integer.valueOf(numv) <= cliente.getVehiculosComprados().size())) {
                                    System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                                    System.out.println("Intente nuevamente...");
                                    numv = entradaPorTeclado.nextLine();
                                }

                                Mantenimiento nuevomantpre = new Mantenimiento(cliente.getVehiculosComprados().get(Integer.valueOf(numv) - 1), cliente);
                                nuevomantpre.setEsEmergente(false);
                                SistemaActual.anadirMantenimiento(nuevomantpre);
                            }
                            break;
                        case "2":
                            String numv2 = "";
                            int countvc2 = 0;
                            if (cliente.getVehiculosComprados().size() == 0) {
                                System.err.println(Color.ANSI_RED + "No ha comprado vehiculos para solicitar un mantenimiento.\n");
                            } else {
                                System.out.println("Elija el vehículo que desea enviar a mantenimiento emergente: ");
                                for (Vehiculo v : cliente.getVehiculosComprados()) {
                                    countvc2 += 1;
                                    System.out.println(Color.ANSI_BLUE + countvc2 + ".- " + v.visorClientesV());
                                }
                                numv2 = entradaPorTeclado.nextLine();
                                while (!(Integer.valueOf(numv2) > 0 && Integer.valueOf(numv2) <= cliente.getVehiculosComprados().size())) {
                                    System.out.println(Color.ANSI_RED + "Elija una opción válida.");
                                    System.out.println("Intente nuevamente...");
                                    numv2 = entradaPorTeclado.nextLine();
                                }

                                Mantenimiento nuevomantemer = new Mantenimiento(cliente.getVehiculosComprados().get(Integer.valueOf(numv2) - 1), cliente);
                                nuevomantemer.setEsEmergente(true);
                                SistemaActual.anadirMantenimiento(nuevomantemer);
                            }

                            break;
                        case "3":
                            break;
                    }
                    ManejoDeArchivos.guardarUsuarios(SistemaActual.getListaUsuarios());
                    ManejoDeArchivos.guardarSolicitudes(SistemaActual.getListaSolicitudes());
                    ManejoDeArchivos.guardarVehiculos(SistemaActual.getListaVehiculos());
                    ManejoDeArchivos.guardarMantenimientos(SistemaActual.getListaMantenimientos());
                    break;
                case "11":
                    break;
            }
        }
    }
}
