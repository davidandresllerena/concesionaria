/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Mantenimientos;

import Herramientas.Color;
import concesionaria.Usuarios.Cliente;
import concesionaria.Vehiculos.Vehiculo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Mantenimiento implements Serializable {

    public static int IDMantenimiento;
    private int Id;
    public static String Estados[] = {"Admitido", "En Reparación", "En Prueba", "Devuelto", "Sin Revision"};
    private static double TarifaPreventivoKm = 0.13;
    private boolean esEmergente;
    private String estado;
    private double precio;
    private Vehiculo Vehiculo;
    private ArrayList<String> RepuestosUsados;
    private Cliente Cliente;

    public Mantenimiento(double precio, Vehiculo Vehiculo, Cliente Cliente) {
        this.estado = Estados[4];
        this.precio = precio;
        this.esEmergente = true;
        this.Cliente = Cliente;
        this.Vehiculo = Vehiculo;
        RepuestosUsados = new ArrayList<>();
        this.Id = IDMantenimiento;
        this.IDMantenimiento++;
    }

    public Mantenimiento(Vehiculo Vehiculo, Cliente Cliente) {
        this.esEmergente = false;
        this.Cliente = Cliente;
        this.estado = Estados[4];
        this.precio = Vehiculo.getKilometraje() * TarifaPreventivoKm;
        this.Vehiculo = Vehiculo;
        RepuestosUsados = new ArrayList<>();
        this.Id = IDMantenimiento;
        this.IDMantenimiento++;
    }

    public int getId() {
        return Id;
    }

    public static String[] getEstados() {
        return Estados;
    }

    public static void setEstados(String[] Estados) {
        Mantenimiento.Estados = Estados;
    }

    public static double getTarifaPreventivoKm() {
        return TarifaPreventivoKm;
    }

    public boolean isEsEmergente() {
        return esEmergente;
    }

    public static int getIDMantenimiento() {
        return IDMantenimiento;
    }

    public void anadirPieza(String Pieza) {
        RepuestosUsados.add(Pieza);
    }

    public void setEsEmergente(boolean esEmergente) {
        this.esEmergente = esEmergente;
    }

    public String getEstado() {
        return estado;
    }

    public Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(Cliente Cliente) {
        this.Cliente = Cliente;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Vehiculo getVehiculo() {
        return Vehiculo;
    }

    public void setVehiculo(Vehiculo Vehiculo) {
        this.Vehiculo = Vehiculo;
    }

    public ArrayList<String> getRepuestosUsados() {
        return RepuestosUsados;
    }

    public void setRepuestosUsados(ArrayList<String> RepuestosUsados) {
        this.RepuestosUsados = RepuestosUsados;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.Id;
        hash = 71 * hash + (this.esEmergente ? 1 : 0);
        hash = 71 * hash + Objects.hashCode(this.estado);
        hash = 71 * hash + (int) (Double.doubleToLongBits(this.precio) ^ (Double.doubleToLongBits(this.precio) >>> 32));
        hash = 71 * hash + Objects.hashCode(this.Vehiculo);
        hash = 71 * hash + Objects.hashCode(this.RepuestosUsados);
        hash = 71 * hash + Objects.hashCode(this.Cliente);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mantenimiento other = (Mantenimiento) obj;
        if (this.Id != other.Id) {
            return false;
        }
        if (this.esEmergente != other.esEmergente) {
            return false;
        }
        if (Double.doubleToLongBits(this.precio) != Double.doubleToLongBits(other.precio)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.Vehiculo, other.Vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.RepuestosUsados, other.RepuestosUsados)) {
            return false;
        }
        if (!Objects.equals(this.Cliente, other.Cliente)) {
            return false;
        }
        return true;
    }

    public String tipoMantenimiento() {
        if (esEmergente == true) {
            return Color.ANSI_BLUE + "Es un mantenimiento emergente, ";
        }
        return Color.ANSI_GREEN + "Es un mantenimiento preventivo, ";
    }

    @Override
    public String toString() {
        return Color.ANSI_RED + "Mantenimiento{" + " Id del Mantenimiento=" + getId() + ", Tipo de mantenimiento=" + tipoMantenimiento() + ", estado=" + estado + ", precio=" + precio + ", Vehiculo=" + Vehiculo + ", RepuestosUsados=" + RepuestosUsados + '}';
    }

}
