/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Usuarios;

import concesionaria.Mantenimientos.Mantenimiento;
import concesionaria.Vehiculos.Vehiculo;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class JefeTaller  extends Empleado{
    private ArrayList<String> certificacionesTecnicas;
    private ArrayList<Vehiculo> vehiculosMantenimiento;

    public JefeTaller(String User, String Contrasena, String Nombres, String Apellidos) {
        super(User, Contrasena, Nombres, Apellidos);
        certificacionesTecnicas = new ArrayList<>();
        vehiculosMantenimiento= new ArrayList<>();
        this.certificacionesTecnicas = certificacionesTecnicas;
        this.vehiculosMantenimiento = vehiculosMantenimiento;
    }

    public ArrayList<String> getCertificacionesTecnicas() {
        return certificacionesTecnicas;
    }

    public void setCertificacionesTecnicas(ArrayList<String> certificacionesTecnicas) {
        this.certificacionesTecnicas = certificacionesTecnicas;
    }

    public ArrayList<Vehiculo> getVehiculosMantenimiento() {
        return vehiculosMantenimiento;
    }

    public void setVehiculosMantenimiento(ArrayList<Vehiculo> vehiculosMantenimiento) {
        this.vehiculosMantenimiento = vehiculosMantenimiento;
    }

    public static int getIDEmpleado() {
        return IDEmpleado;
    }


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.certificacionesTecnicas);
        hash = 47 * hash + Objects.hashCode(this.vehiculosMantenimiento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JefeTaller other = (JefeTaller) obj;
        if (!Objects.equals(this.certificacionesTecnicas, other.certificacionesTecnicas)) {
            return false;
        }
        if (!Objects.equals(this.vehiculosMantenimiento, other.vehiculosMantenimiento)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JefeTaller{" + super.presentarInfo()+", ID="+ getId() +", certificacionesTecnicas=" + certificacionesTecnicas + ", vehiculosMantenimiento=" + vehiculosMantenimiento + '}';
    }
    
    
        
    
    
    /**
     * Registra partes y repuestos utilizados en un mantenimiento dado
     */
    public Mantenimiento registrarRepuesto(ArrayList<Mantenimiento> Lmantenimientos,int Id ,String Pieza){
        for(Mantenimiento m: Lmantenimientos){
            if(m.getIDMantenimiento()==Id){
                m.anadirPieza(Pieza);
            }
        }
        return null;
    }
    
}
