/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Usuarios;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public abstract class Usuario implements Serializable {

    private String User;
    private String Contrasena;
    private String Nombres;
    private String Apellidos;

    public Usuario(String User, String Contrasena, String Nombres, String Apellidos) {
        this.User = User;
        this.Contrasena = Contrasena;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;

    }

    public String getTipoUsuario() {
        return this.getClass().getName();
    }

    public String getUser() {
        return User;
    }

    public void setUser(String User) {
        this.User = User;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String Contrasena) {
        this.Contrasena = Contrasena;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.User);
        hash = 67 * hash + Objects.hashCode(this.Contrasena);
        hash = 67 * hash + Objects.hashCode(this.Nombres);
        hash = 67 * hash + Objects.hashCode(this.Apellidos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.User, other.User)) {
            return false;
        }
        if (!Objects.equals(this.Contrasena, other.Contrasena)) {
            return false;
        }
        if (!Objects.equals(this.Nombres, other.Nombres)) {
            return false;
        }
        if (!Objects.equals(this.Apellidos, other.Apellidos)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "User=" + User + ", Contrasena=" + Contrasena + ", Nombres=" + Nombres + ", Apellidos=" + Apellidos + '}';
    }

    public String presentarInfo() {
        return "User=" + User + ", Contrasena=" + Contrasena + ", Nombres=" + Nombres + ", Apellidos=" + Apellidos;
    }

    public String presentarInfoC() {
        return "Nombres=" + Nombres + ", Apellidos=" + Apellidos;
    }

}
