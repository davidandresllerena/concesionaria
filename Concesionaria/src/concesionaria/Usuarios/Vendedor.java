/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Usuarios;

import concesionaria.Mantenimientos.Mantenimiento;
import concesionaria.Solicitudes.Cotizacion;
import concesionaria.Solicitudes.Solicitud;
import concesionaria.Vehiculos.Vehiculo;
import java.util.ArrayList;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Vendedor extends Empleado {

    private ArrayList<Solicitud> Cotizaciones;
    private ArrayList<Vehiculo> Ventas;
    public final Double BonoVenta = 100.0;

    public Vendedor(String User, String Contrasena, String Nombres, String Apellidos) {
        super(User, Contrasena, Nombres, Apellidos);
        Cotizaciones = new ArrayList<>();
        this.Cotizaciones = Cotizaciones;
        Ventas = new ArrayList<>();
        this.Ventas = Ventas;
    }

    public ArrayList<Vehiculo> getVentas() {
        return Ventas;
    }

    public void setVentas(ArrayList<Vehiculo> Ventas) {
        this.Ventas = Ventas;
    }

    public ArrayList<Solicitud> getCotizaciones() {
        return Cotizaciones;
    }

    public void setCotizaciones(ArrayList<Solicitud> Cotizaciones) {
        this.Cotizaciones = Cotizaciones;
    }

    public static int getIDEmpleado() {
        return IDEmpleado;
    }

    /**
     * Ingresa nuevos vehículos al stock de la concesionaria.
     */
    public void ingresarNuevoVehiculo(ArrayList<Vehiculo> ListaV, Vehiculo v) {
        ListaV.add(v);
    }

    /**
     * Sugiere modelos alternativos de vehículos al rechazar cotizaciones
     */
    public void sugerirVehiculos(ArrayList<Vehiculo> LisV, Vehiculo v) {
        System.out.println(v.getTipoVehiculo());
        for (Vehiculo ve : LisV) {
            if ((!v.equals(ve)) && ve.getTipoVehiculo().equals(v.getTipoVehiculo())) {
                System.out.println(ve.toString());
            }
        }
    }

    /**
     * Modifica los detalles de los vehículos disponibles para venta.
     */
    public void modificarDatosVehiculos() {

    }

    /**
     * Consultar si alguno de los vehículos que ha vendido se encuentra
     * actualmente enmantenimiento
     */
    public void consultarVehiculoenReparacion(ArrayList<Mantenimiento> ListaM) {
        for (Mantenimiento m : ListaM) {
            for (Vehiculo v : Ventas) {
                if (v.equals(m.getVehiculo())) {
                    System.out.println(m.toString());
                }
            }
        }

    }

    @Override
    public String toString() {
        return "Vendedor{" + super.presentarInfo() + ", ID=" + getId() + ", Cotizaciones=" + Cotizaciones + ", Ventas=" + Ventas + '}';
    }

    public String toStringCliente() {
        return "Vendedor{" + super.presentarInfoC() + '}';
    }

    public void anadirCotizacion(Solicitud s) {
        Cotizaciones.add(s);
    }

    public Double getBonoVenta() {
        if (Ventas.size() > 3) {
            return BonoVenta * Ventas.size();
        }
        return 0.0;
    }

}
