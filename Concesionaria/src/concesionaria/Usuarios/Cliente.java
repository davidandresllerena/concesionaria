/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Usuarios;

import static concesionaria.Usuarios.Empleado.IDEmpleado;
import concesionaria.Usuarios.Usuario;
import concesionaria.Vehiculos.Vehiculo;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Cliente extends Usuario {
    private String CI;
    private String Ocupacion;
    private Double Ingresos;
    private ArrayList<Vehiculo> VehiculosComprados;
    private ArrayList<Vehiculo> WishList;

    public Cliente(String CI, String Ocupacion, Double Ingresos, String User, String Contrasena, String Nombres, String Apellidos) {
        super(User, Contrasena, Nombres, Apellidos);
        this.CI = CI;
        this.Ocupacion = Ocupacion;
        this.Ingresos = Ingresos;
        VehiculosComprados = new ArrayList<>();
        this.VehiculosComprados = VehiculosComprados;
        WishList= new ArrayList<>();
        this.WishList=  WishList;
    }

    public String getCI() {
        return CI;
    }

    public void setCI(String CI) {
        this.CI = CI;
    }

    public String getOcupacion() {
        return Ocupacion;
    }

    public void setOcupacion(String Ocupacion) {
        this.Ocupacion = Ocupacion;
    }

    public Double getIngresos() {
        return Ingresos;
    }

    public void setIngresos(Double Ingresos) {
        this.Ingresos = Ingresos;
    }

    public ArrayList<Vehiculo> getVehiculosComprados() {
        return VehiculosComprados;
    }

    public void setVehiculosComprados(ArrayList<Vehiculo> VehiculosComprados) {
        this.VehiculosComprados = VehiculosComprados;
    }

    public ArrayList<Vehiculo> getWishList() {
        return WishList;
    }

    public void setWishList(ArrayList<Vehiculo> WishList) {
        this.WishList = WishList;
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.CI);
        hash = 23 * hash + Objects.hashCode(this.Ocupacion);
        hash = 23 * hash + Objects.hashCode(this.Ingresos);
        hash = 23 * hash + Objects.hashCode(this.VehiculosComprados);
        hash = 23 * hash + Objects.hashCode(this.WishList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.CI, other.CI)) {
            return false;
        }
        if (!Objects.equals(this.Ocupacion, other.Ocupacion)) {
            return false;
        }
        if (!Objects.equals(this.Ingresos, other.Ingresos)) {
            return false;
        }
        if (!Objects.equals(this.VehiculosComprados, other.VehiculosComprados)) {
            return false;
        }
        if (!Objects.equals(this.WishList, other.WishList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cliente{" + super.presentarInfo()+", CI=" + CI + ", Ocupacion=" + Ocupacion + ", Ingresos=" + Ingresos + ", VehiculosComprados=" + VehiculosComprados + ", WishList=" + WishList + '}';
    }

     
    /**
     * Modifica la informacion de cliente
     */
    public void modificarInformacion(String CI, String Ocupacion, Double Ingresos, String User, String Contrasena, String Nombres, String Apellidos){
        this.CI= CI;
        this.Ocupacion= Ocupacion;
        this.Ingresos=Ingresos;
        super.setUser(User);
        super.setContrasena(Contrasena);
        super.setNombres(Nombres);
        super.setApellidos(Apellidos);
    }
    
}
