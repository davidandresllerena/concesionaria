/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Usuarios;

import Herramientas.Color;
import Herramientas.Validaciones;
import static Herramientas.Validaciones.isNumeric;
import static Herramientas.Validaciones.validarContrasena;
import concesionaria.Mantenimientos.Mantenimiento;
import concesionaria.Solicitudes.Compra;
import concesionaria.Solicitudes.Solicitud;
import concesionaria.Vehiculos.Vehiculo;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Supervisor extends Empleado {

    private ArrayList<String> certificacionesAcademicas;
    private ArrayList<Compra> solicitudesCompra;

    public Supervisor(String User, String Contrasena, String Nombres, String Apellidos) {
        super(User, Contrasena, Nombres, Apellidos);
        certificacionesAcademicas = new ArrayList<>();
        this.certificacionesAcademicas = certificacionesAcademicas;
        solicitudesCompra = new ArrayList<>();
        this.solicitudesCompra = solicitudesCompra;
    }

    public ArrayList<String> getCertificacionesAcademicas() {
        return certificacionesAcademicas;
    }

    public void setCertificacionesAcademicas(ArrayList<String> certificacionesAcademicas) {
        this.certificacionesAcademicas = certificacionesAcademicas;
    }

    public ArrayList<Compra> getSolicitudesCompra() {
        return solicitudesCompra;
    }

    public void setSolicitudesCompra(ArrayList<Compra> solicitudesCompra) {
        this.solicitudesCompra = solicitudesCompra;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.certificacionesAcademicas);
        hash = 67 * hash + Objects.hashCode(this.solicitudesCompra);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Supervisor other = (Supervisor) obj;
        if (!Objects.equals(this.certificacionesAcademicas, other.certificacionesAcademicas)) {
            return false;
        }
        if (!Objects.equals(this.solicitudesCompra, other.solicitudesCompra)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Supervisor{" + super.presentarInfo() + ", ID=" + getId() + ", certificacionesAcademicas=" + certificacionesAcademicas + ", solicitudesCompra=" + solicitudesCompra + '}';
    }

    /**
     * Consulta los detalles de los empleados de la concesionaria.
     *
     * @return
     */
    public void consultarEmpleado(ArrayList<Usuario> Usuarios) {
        for (Usuario user : Usuarios) {
            if (user instanceof Empleado) {
                Empleado emp = (Empleado) user;
                System.out.println(Color.ANSI_PURPLE + emp.toString());
            }
        }
    }

    /**
     * Devuelves cuántos vehículos ha vendido un vendedor específico.
     *
     * @return
     */
    public void consultarVehiculosVendidosPorVendedor(ArrayList<Usuario> ListaUsuarios) {
        for (Usuario u : ListaUsuarios) {
            if (u instanceof Vendedor) {
                Vendedor v = (Vendedor) u;
                int tam = v.getVentas().size();
                System.out.println(Color.ANSI_PURPLE + "Vendedor: " + v.getNombres() + " " + v.getApellidos() + "| Carros Vendidos: " + tam);
            }
        }
    }

    /**
     * Consulta los vehículos que se encuentran en reparación
     *
     * @return
     */
    public void consultarVehiculosEnReparacion(ArrayList<Mantenimiento> ListaMante) {
        for (Mantenimiento m : ListaMante) {
            System.out.println(Color.ANSI_PURPLE + m.toString());
        }
    }

    /**
     * Consulta quien vendio los vehiculos
     *
     * @return
     */
    public void consultarQuienVendio(ArrayList<Solicitud> Solicitudes) {

        for (Solicitud sol : Solicitudes) {
            if (sol instanceof Compra) {
                if (sol.isEstaAprobada() == true) {
                    System.out.println(sol.getVehiculo().toString() + ((Compra) sol).getVendedor().toString());
                }
            }
        }
    }

    /**
     * Ingresa nuevos vehículos al stock de la concesionaria.
     *
     */
    public void agregarNuevosVehiculos(ArrayList<Vehiculo> Vehiculos, Vehiculo V) {
        Vehiculos.add(V);
    }

    public void anadirSolicitudCompra(Compra Sc) {
        solicitudesCompra.add(Sc);
    }
}
