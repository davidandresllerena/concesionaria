/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Solicitudes;

import Herramientas.Color;
import concesionaria.Usuarios.Vendedor;
import concesionaria.Vehiculos.Vehiculo;
import concesionaria.Usuarios.Cliente;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Cotizacion extends Solicitud {

    public Cotizacion(boolean estaAprobada, Cliente Cliente, Vendedor Vendedor, Vehiculo Vehiculo) {
        super(estaAprobada, Cliente, Vendedor, Vehiculo);
    }

    public Cotizacion(Cliente Cliente, Vendedor Vendedor, Vehiculo Vehiculo) {
        super(Cliente, Vendedor, Vehiculo);
    }

    @Override
    public String toString() {
        return "Cotizacion{" + super.toString() + '}';
    }

    @Override
    public String toStringCliente() {
        return "Cotizacion{" + "Estado=" + estado() + motivoRechazo() + "Vehiculo=" + datosVehiculos() + '}';
    }

}
