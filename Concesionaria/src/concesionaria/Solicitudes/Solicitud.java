/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Solicitudes;

import Herramientas.Color;
import concesionaria.Usuarios.Cliente;
import concesionaria.Usuarios.Vendedor;
import concesionaria.Vehiculos.Vehiculo;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public abstract class Solicitud implements Serializable {

    private boolean estaAprobada;
    private Cliente Cliente;
    private Vehiculo Vehiculo;
    private String MotivoRechazo;
    private static int IdSolicitud;
    private Vendedor Vendedor;

    public Solicitud(boolean estaAprobada, Cliente Cliente, Vendedor Vendedor, Vehiculo Vehiculo) {
        this.estaAprobada = estaAprobada;
        this.Cliente = Cliente;
        this.MotivoRechazo = Color.ANSI_YELLOW + "SU SOLICITUD ESTÁ EN REVISION!";
        this.Vehiculo = Vehiculo;
        this.IdSolicitud += 1;
        IdSolicitud++;
    }

    public Solicitud(Cliente Cliente, Vendedor Vendedor, Vehiculo Vehiculo) {
        this.estaAprobada = false;
        this.Cliente = Cliente;
        this.MotivoRechazo = Color.ANSI_YELLOW + "SU SOLICITUD ESTÁ EN REVISION!";
        this.Vehiculo = Vehiculo;
        this.IdSolicitud += 1;
        IdSolicitud++;
    }

    public String getMotivoRechazo() {
        return MotivoRechazo;
    }

    public void setMotivoRechazo(String MotivoRechazo) {
        this.MotivoRechazo = MotivoRechazo;
    }

    public Vendedor getVendedor() {
        return Vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.Vendedor = vendedor;
    }

    public static int getIdSolicitud() {
        return IdSolicitud;
    }

    public boolean isEstaAprobada() {
        return estaAprobada;
    }

    public void setEstaAprobada(boolean estaAprobada) {
        this.estaAprobada = estaAprobada;
    }

    public Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(Cliente Cliente) {
        this.Cliente = Cliente;
    }

    public Vehiculo getVehiculo() {
        return Vehiculo;
    }

    public void setVehiculo(Vehiculo Vehiculo) {
        this.Vehiculo = Vehiculo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.estaAprobada ? 1 : 0);
        hash = 47 * hash + Objects.hashCode(this.Cliente);
        hash = 47 * hash + Objects.hashCode(this.Vehiculo);
        hash = 47 * hash + Objects.hashCode(this.MotivoRechazo);
        hash = 47 * hash + Objects.hashCode(this.Vendedor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Solicitud other = (Solicitud) obj;
        if (this.estaAprobada != other.estaAprobada) {
            return false;
        }
        if (!Objects.equals(this.MotivoRechazo, other.MotivoRechazo)) {
            return false;
        }
        if (!Objects.equals(this.Cliente, other.Cliente)) {
            return false;
        }
        if (!Objects.equals(this.Vehiculo, other.Vehiculo)) {
            return false;
        }
        if (!Objects.equals(this.Vendedor, other.Vendedor)) {
            return false;
        }
        return true;
    }

    public String motivoRechazo() {
        if (isEstaAprobada() == true) {
            return ", ";
        }
        return ", Motivo del Rechazo: " + MotivoRechazo + ", ";
    }

    public String estado() {
        if (isEstaAprobada() == true) {
            return Color.ANSI_CYAN + "Está Aprobada!";
        }
        return Color.ANSI_RED + "No está aprobada";
    }

    public String datosVehiculos() {
        if (isEstaAprobada() == true) {
            return getVehiculo().toString();
        }
        return getVehiculo().visorClientes();
    }

    public abstract String toStringCliente();

    @Override
    public String toString() {
        return "Solicitud{" + "estaAprobada=" + estaAprobada + "Cliente=" + Cliente + ", Vehiculo=" + Vehiculo + ", Vendedor=" + Vendedor + '}';
    }

}
