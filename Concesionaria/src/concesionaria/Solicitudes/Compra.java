package concesionaria.Solicitudes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import concesionaria.Usuarios.Cliente;
import concesionaria.Usuarios.Supervisor;
import concesionaria.Usuarios.Vendedor;
import concesionaria.Vehiculos.Vehiculo;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Compra extends Solicitud {

    private Supervisor Supervisor;

    public Compra(Supervisor Supervisor, boolean estaAprobada, Cliente Cliente, Vendedor Vendedor, Vehiculo Vehiculo) {
        super(estaAprobada, Cliente, Vendedor, Vehiculo);
        this.Supervisor = Supervisor;
    }

    public Compra(Supervisor Supervisor, Cliente Cliente, Vendedor Vendedor, Vehiculo Vehiculo) {
        super(Cliente, Vendedor, Vehiculo);
        this.Supervisor = Supervisor;
    }

    public Supervisor getSupervisor() {
        return Supervisor;
    }

    public void setSupervisor(Supervisor Supervisor) {
        this.Supervisor = Supervisor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.Supervisor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Compra other = (Compra) obj;
        if (!Objects.equals(this.Supervisor, other.Supervisor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Compra{" + "Supervisor=" + Supervisor + '}';
    }

    @Override
    public String toStringCliente() {
        return "Solicitud de Compra{" + "Estado=" + estado() + motivoRechazo() + "Vehiculo=" + datosVehiculos() + '}';
    }

}
