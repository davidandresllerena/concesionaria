/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria;

import Herramientas.Color;
import Herramientas.ManejoDeArchivos;
import concesionaria.Mantenimientos.Mantenimiento;
import concesionaria.Solicitudes.Compra;
import concesionaria.Solicitudes.Cotizacion;
import concesionaria.Solicitudes.Solicitud;
import concesionaria.Usuarios.Cliente;
import concesionaria.Usuarios.Empleado;
import concesionaria.Usuarios.JefeTaller;
import concesionaria.Usuarios.Supervisor;
import concesionaria.Usuarios.Usuario;
import concesionaria.Usuarios.Vendedor;
import concesionaria.Vehiculos.Automovil;
import concesionaria.Vehiculos.Camion;
import concesionaria.Vehiculos.Motocicleta;
import concesionaria.Vehiculos.Tractor;
import concesionaria.Vehiculos.Vehiculo;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Concesionaria {

    private ArrayList<Mantenimiento> ListaMantenimientos;
    private ArrayList<Solicitud> ListaSolicitudes;
    private ArrayList<Usuario> ListaUsuarios;
    private ArrayList<Vehiculo> ListaVehiculos;

    public Concesionaria() {
        ListaVehiculos = ManejoDeArchivos.cargarVehiculos();
        ListaMantenimientos = ManejoDeArchivos.cargarMantenimientos();
        ListaUsuarios = ManejoDeArchivos.cargarUsuarios();
        ListaSolicitudes = ManejoDeArchivos.cargarSolicitudes();

    }

    public void anadirMantenimiento(Mantenimiento m) {
        ListaMantenimientos.add(m);
    }

    public void anadirSolicitud(Solicitud s) {
        ListaSolicitudes.add(s);
    }

    public void anadirUsuario(Usuario u) {
        ListaUsuarios.add(u);
    }

    public void anadirVehiculos(Vehiculo v) {
        ListaVehiculos.add(v);
    }

    public void removerMantenimiento(Mantenimiento m) {
        ListaMantenimientos.remove(m);
    }

    public void removerSolicitud(Solicitud s) {
        ListaSolicitudes.remove(s);
    }

    public void removerUsuario(Usuario u) {
        ListaUsuarios.remove(u);
    }

    public void removerVehiculos(Vehiculo v) {
        ListaVehiculos.remove(v);
    }

    public ArrayList<Mantenimiento> getListaMantenimientos() {
        return ListaMantenimientos;
    }

    public void setListaMantenimientos(ArrayList<Mantenimiento> ListaMantenimientos) {
        this.ListaMantenimientos = ListaMantenimientos;
    }

    public ArrayList<Solicitud> getListaSolicitudes() {
        return ListaSolicitudes;
    }

    public void setListaSolicitudes(ArrayList<Solicitud> ListaSolicitudes) {
        this.ListaSolicitudes = ListaSolicitudes;
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return ListaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> ListaUsuarios) {
        this.ListaUsuarios = ListaUsuarios;
    }

    public ArrayList<Vehiculo> getListaVehiculos() {
        return ListaVehiculos;
    }

    public void setListaVehiculos(ArrayList<Vehiculo> ListaVehiculos) {
        this.ListaVehiculos = ListaVehiculos;
    }

    public Empleado buscarEmpleadoId(int Id) {
        for (Usuario user : ListaUsuarios) {
            if (user instanceof Empleado) {
                Empleado empn = (Empleado) user;
                if (empn.getId() == Id) {
                    return empn;
                }
            }
        }
        return null;
    }

    public void verAutomoviles() {
        for (Vehiculo v : ListaVehiculos) {
            if (v instanceof Automovil && (v.isEstaSolicitado() == false)) {
                Automovil au = (Automovil) v;
                System.out.println(Color.ANSI_CYAN + au.visorClientes());
            }
        }
    }

    public int vehiculosDisponibles() {
        int disp = 0;
        for (Vehiculo v : ListaVehiculos) {
            if (v.isEstaSolicitado() == false) {
                disp += 1;
            }
        }
        return disp;
    }

    public void verCamiones() {
        for (Vehiculo v : ListaVehiculos) {
            if (v instanceof Camion && (v.isEstaSolicitado() == false)) {
                Camion au = (Camion) v;
                System.out.println(Color.ANSI_CYAN + au.visorClientes());
            }
        }
    }

    public void verMotos() {
        for (Vehiculo v : ListaVehiculos) {
            if (v instanceof Motocicleta && (v.isEstaSolicitado() == false)) {
                Motocicleta au = (Motocicleta) v;
                System.out.println(Color.ANSI_CYAN + au.visorClientes());
            }
        }
    }

    public void verTractores() {
        for (Vehiculo v : ListaVehiculos) {
            if (v instanceof Tractor && (v.isEstaSolicitado() == false)) {
                Tractor au = (Tractor) v;
                System.out.println(Color.ANSI_CYAN + au.visorClientes());
            }
        }
    }

    public void cambioEstadoSolicitud(Solicitud ss, Boolean boo, String msj) {
        ListaSolicitudes.stream().filter((s) -> (s.equals(ss))).map((s) -> {
            s.setEstaAprobada(boo);
            return s;
        }).forEachOrdered((s) -> {
            s.setMotivoRechazo(msj);
        });
    }

    public void cambioEstadoSolicitudSi(Supervisor s, Boolean boo, String msj, String Pos) {
        int p = Integer.valueOf(Pos);

        s.getSolicitudesCompra().get(p - 1).setEstaAprobada(boo);
        s.getSolicitudesCompra().get(p - 1).setMotivoRechazo(msj);
        ListaVehiculos.remove(s.getSolicitudesCompra().get(p - 1).getVehiculo());

    }

    public ArrayList<Vendedor> getListaVendedores() {
        ArrayList<Vendedor> lv = new ArrayList<>();
        ListaUsuarios.stream().filter((user) -> (user instanceof Vendedor)).forEachOrdered((user) -> {
            lv.add(((Vendedor) user));
        });
        return lv;
    }

    public ArrayList<Supervisor> getListaSupervisores() {
        ArrayList<Supervisor> s = new ArrayList<>();
        for (Usuario user : ListaUsuarios) {
            if (user instanceof Supervisor) {
                s.add(((Supervisor) user));
            }
        }
        return s;
    }

    public Vendedor esMismoVendedor(Vendedor v) {
        for (Usuario user : ListaUsuarios) {
            if (user instanceof Vendedor && ((Vendedor) user).equals(v)) {
                return (Vendedor) user;
            }
        }
        return null;
    }

    public Cliente esMismoCliente(Cliente c) {
        for (Usuario user : ListaUsuarios) {
            if (user instanceof Cliente && ((Vendedor) user).equals(c)) {
                return (Cliente) user;
            }
        }
        return null;
    }

    public Mantenimiento buscarMantenimientoId(int Id) {
        for (Mantenimiento m : ListaMantenimientos) {
            if (m.getId() == Id) {
                return m;
            }
        }
        return null;
    }

    public void verMantenimientos() {
        for (Mantenimiento m : ListaMantenimientos) {
            System.out.println(Color.ANSI_PURPLE + m.toString());
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.ListaMantenimientos);
        hash = 53 * hash + Objects.hashCode(this.ListaSolicitudes);
        hash = 53 * hash + Objects.hashCode(this.ListaUsuarios);
        hash = 53 * hash + Objects.hashCode(this.ListaVehiculos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Concesionaria other = (Concesionaria) obj;
        if (!Objects.equals(this.ListaMantenimientos, other.ListaMantenimientos)) {
            return false;
        }
        if (!Objects.equals(this.ListaSolicitudes, other.ListaSolicitudes)) {
            return false;
        }
        if (!Objects.equals(this.ListaUsuarios, other.ListaUsuarios)) {
            return false;
        }
        if (!Objects.equals(this.ListaVehiculos, other.ListaVehiculos)) {
            return false;
        }
        return true;
    }

}
