/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Vehiculos;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Motocicleta extends Vehiculo {

    public static String Categorias[] = {"deportivo", "scooter", "Todo Terreno"};
    private String Categoria;

    public Motocicleta(String Categoria, String Marca, String Modelo, int AñoFabricacion, boolean MotorGasolina, double Precio) {
        super(Marca, Modelo, AñoFabricacion, MotorGasolina, Precio);
        this.Categoria = Categoria;
        super.setNumeroLlantas(2);
    }

    public static String[] getCategorias() {
        return Categorias;
    }

    public static void setCategorias(String[] Categorias) {
        Motocicleta.Categorias = Categorias;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.Categoria);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Motocicleta other = (Motocicleta) obj;
        if (!Objects.equals(this.Categoria, other.Categoria)) {
            return false;
        }
        return true;
    }

    public String visorClientes() {
        return "Motocicleta{" + super.visorClientesV() + "}";
    }

    @Override
    public String toString() {
        return "Motocicleta{" + super.toString() + "Categoria=" + Categoria + '}';
    }

    @Override
    public String datosMantenimiento() {
        return "Motocicleta{" + super.visorClientesM() + "}";
    }
}
