/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Vehiculos;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Automovil extends Vehiculo {

    private int numeroAsientos;
    private boolean esConvertible;
    private boolean tieneCamaraRetro;

    public Automovil(int numeroAsientos, boolean esConvertible, boolean tieneCamaraRetro, String Marca, String Modelo, int AñoFabricacion, boolean MotorGasolina, double Precio) {
        super(Marca, Modelo, AñoFabricacion, MotorGasolina, Precio);
        this.numeroAsientos = numeroAsientos;
        this.esConvertible = esConvertible;
        this.tieneCamaraRetro = tieneCamaraRetro;
        super.setNumeroLlantas(4);
    }

    public int getNumeroAsientos() {
        return numeroAsientos;
    }

    public void setNumeroAsientos(int numeroAsientos) {
        this.numeroAsientos = numeroAsientos;
    }

    public boolean isEsConvertible() {
        return esConvertible;
    }

    public void setEsConvertible(boolean esConvertible) {
        this.esConvertible = esConvertible;
    }

    public boolean isTieneCamaraRetro() {
        return tieneCamaraRetro;
    }

    public void setTieneCamaraRetro(boolean tieneCamaraRetro) {
        this.tieneCamaraRetro = tieneCamaraRetro;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.numeroAsientos;
        hash = 29 * hash + (this.esConvertible ? 1 : 0);
        hash = 29 * hash + (this.tieneCamaraRetro ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Automovil other = (Automovil) obj;
        if (this.numeroAsientos != other.numeroAsientos) {
            return false;
        }
        if (this.esConvertible != other.esConvertible) {
            return false;
        }
        if (this.tieneCamaraRetro != other.tieneCamaraRetro) {
            return false;
        }
        return true;
    }

    public String visorClientes() {
        return "Automovil{" + super.visorClientesV() + "}";
    }

    @Override
    public String toString() {
        return "Automovil{" + super.toString() + "Numero de Llantas=" + super.getNumeroLlantas() + "numeroAsientos=" + numeroAsientos + ", esConvertible=" + esConvertible + ", tieneCamaraRetro=" + tieneCamaraRetro + '}';
    }

    @Override
    public String datosMantenimiento() {
        return "Automovil{" + super.visorClientesM() + "}";
    }

}
