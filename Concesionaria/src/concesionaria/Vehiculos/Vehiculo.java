/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Vehiculos;

import Herramientas.Color;
import concesionaria.Usuarios.Vendedor;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public abstract class Vehiculo implements Serializable {

    private String Marca;
    private String Modelo;
    private int AñoFabricacion;
    private boolean MotorGasolina;
    private int NumeroLlantas;
    private Vendedor Vendedor;
    private boolean estaEnMantenimiento;
    private double Kilometraje;
    private double Precio;
    private boolean estaSolicitado;

    public Vehiculo(String Marca, String Modelo, int AñoFabricacion, boolean MotorGasolina, double Precio) {
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.AñoFabricacion = AñoFabricacion;
        this.MotorGasolina = MotorGasolina;
        this.Vendedor = null;
        this.estaEnMantenimiento = false;
        this.estaSolicitado = false;
        this.Kilometraje = 0;
        this.Precio = Precio;
    }

    public Vehiculo(String Marca, String Modelo, int AñoFabricacion, boolean MotorGasolina, int NumeroLlantas, double Precio) {
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.AñoFabricacion = AñoFabricacion;
        this.MotorGasolina = MotorGasolina;
        this.NumeroLlantas = NumeroLlantas;
        this.Vendedor = null;
        this.estaEnMantenimiento = false;
        this.estaSolicitado = false;
        this.Kilometraje = 0;
        this.Precio = Precio;
    }

    public Vehiculo(String Marca, String Modelo, int AñoFabricacion, double Precio) {
        this.Marca = Marca;
        this.Modelo = Modelo;
        this.AñoFabricacion = AñoFabricacion;
        this.Vendedor = null;
        this.estaEnMantenimiento = false;
        this.estaSolicitado = false;
        this.Kilometraje = 0;
        this.Precio = Precio;
    }

    public String getTipoVehiculo() {
        return this.getClass().getName();
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public int getAñoFabricacion() {
        return AñoFabricacion;
    }

    public void setAñoFabricacion(int AñoFabricacion) {
        this.AñoFabricacion = AñoFabricacion;
    }

    public boolean isMotorGasolina() {
        return MotorGasolina;
    }

    public void setMotorGasolina(boolean MotorGasolina) {
        this.MotorGasolina = MotorGasolina;
    }

    public int getNumeroLlantas() {
        return NumeroLlantas;
    }

    public void setNumeroLlantas(int NumeroLlantas) {
        this.NumeroLlantas = NumeroLlantas;
    }

    public Vendedor getVendedor() {
        return Vendedor;
    }

    public void setVendedor(Vendedor Vendedor) {
        this.Vendedor = Vendedor;
    }

    public boolean isEstaEnMantenimiento() {
        return estaEnMantenimiento;
    }

    public void setEstaEnMantenimiento(boolean estaEnMantenimiento) {
        this.estaEnMantenimiento = estaEnMantenimiento;
    }

    public double getKilometraje() {
        return Kilometraje;
    }

    public void setKilometraje(double Kilometraje) {
        this.Kilometraje = Kilometraje;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }

    public boolean isEstaSolicitado() {
        return estaSolicitado;
    }

    public void setEstaSolicitado(boolean estaSolicitado) {
        this.estaSolicitado = estaSolicitado;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.Marca);
        hash = 47 * hash + Objects.hashCode(this.Modelo);
        hash = 47 * hash + this.AñoFabricacion;
        hash = 47 * hash + (this.MotorGasolina ? 1 : 0);
        hash = 47 * hash + this.NumeroLlantas;
        hash = 47 * hash + Objects.hashCode(this.Vendedor);
        hash = 47 * hash + (this.estaEnMantenimiento ? 1 : 0);
        hash = 47 * hash + (int) (Double.doubleToLongBits(this.Kilometraje) ^ (Double.doubleToLongBits(this.Kilometraje) >>> 32));
        hash = 47 * hash + (int) (Double.doubleToLongBits(this.Precio) ^ (Double.doubleToLongBits(this.Precio) >>> 32));
        hash = 47 * hash + (this.estaSolicitado ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (this.AñoFabricacion != other.AñoFabricacion) {
            return false;
        }
        if (this.MotorGasolina != other.MotorGasolina) {
            return false;
        }
        if (this.NumeroLlantas != other.NumeroLlantas) {
            return false;
        }
        if (this.estaEnMantenimiento != other.estaEnMantenimiento) {
            return false;
        }
        if (Double.doubleToLongBits(this.Kilometraje) != Double.doubleToLongBits(other.Kilometraje)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Precio) != Double.doubleToLongBits(other.Precio)) {
            return false;
        }
        if (this.estaSolicitado != other.estaSolicitado) {
            return false;
        }
        if (!Objects.equals(this.Marca, other.Marca)) {
            return false;
        }
        if (!Objects.equals(this.Modelo, other.Modelo)) {
            return false;
        }
        if (!Objects.equals(this.Vendedor, other.Vendedor)) {
            return false;
        }
        return true;
    }

    public String visorClientesV() {
        return "Vehiculo{" + esNuevo() + "Marca=" + Marca + ", Modelo=" + Modelo + ", A\u00f1oFabricacion=" + AñoFabricacion + "}";
    }

    public abstract String visorClientes();

    public abstract String datosMantenimiento();

    public String esNuevo() {
        if (Kilometraje == 0.0) {
            return Color.ANSI_GREEN + " El vehiculo es nuevo, 0 Kilómetros!, ";
        }
        return Color.ANSI_YELLOW + " El vehiculo es usado y ha recorrido: " + Kilometraje + " Kilómetros, ";
    }

    public String estaMantenimiento() {
        if (estaEnMantenimiento == true) {
            return ", El vehículo se encuentra en mantenimiento, ";
        }
        return ", ";
    }

    public String visorClientesM() {
        return "Vehiculo{" + estaMantenimiento() + Kilometraje + " Kilómetros, " + "Marca=" + Marca + ", Modelo=" + Modelo + ", A\u00f1oFabricacion=" + AñoFabricacion + "}";
    }

    @Override
    public String toString() {
        return "Vehiculo{" + esNuevo() + "Marca=" + Marca + ", Modelo=" + Modelo + ", A\u00f1oFabricacion=" + AñoFabricacion + ", MotorGasolina=" + MotorGasolina + ", NumeroLlantas=" + NumeroLlantas + ", Kilometraje=" + Kilometraje + ", Precio=" + Precio + ", estaSolicitado=" + estaSolicitado + '}';
    }

}
