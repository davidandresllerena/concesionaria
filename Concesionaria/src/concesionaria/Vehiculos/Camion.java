/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Vehiculos;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Camion extends Vehiculo {

    private double capacidadCarga;
    private int numeroEjes;

    public Camion(double capacidadCarga, String Marca, String Modelo, int AñoFabricacion, boolean MotorGasolina, int NumeroLlantas, double Precioo) {
        super(Marca, Modelo, AñoFabricacion, MotorGasolina, NumeroLlantas, Precioo);
        this.capacidadCarga = capacidadCarga;
        this.numeroEjes = super.getNumeroLlantas() / 2;
    }

    public double getCapacidadCarga() {
        return capacidadCarga;
    }

    public void setCapacidadCarga(double capacidadCarga) {
        this.capacidadCarga = capacidadCarga;
    }

    public int getNumeroEjes() {
        return numeroEjes;
    }

    public void setNumeroEjes(int numeroEjes) {
        this.numeroEjes = numeroEjes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.capacidadCarga) ^ (Double.doubleToLongBits(this.capacidadCarga) >>> 32));
        hash = 97 * hash + this.numeroEjes;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Camion other = (Camion) obj;
        if (Double.doubleToLongBits(this.capacidadCarga) != Double.doubleToLongBits(other.capacidadCarga)) {
            return false;
        }
        if (this.numeroEjes != other.numeroEjes) {
            return false;
        }
        return true;
    }

    public String visorClientes() {
        return "Camion{" + super.visorClientesV() + "}";
    }

    @Override
    public String toString() {
        return "Camion{" + super.toString() + "capacidadCarga=" + capacidadCarga + ", numeroEjes=" + numeroEjes + '}';
    }

    @Override
    public String datosMantenimiento() {
        return "Camion{" + super.visorClientesM() + "}";
    }

}
