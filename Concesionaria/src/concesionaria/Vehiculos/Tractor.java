/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concesionaria.Vehiculos;

/**
 *
 * @author Grupo 7 (Pedro Conforme,David Llerena,Ariel Velez)
 */
public class Tractor extends Vehiculo {

    private boolean usoAgricola;
    private boolean transimisionHidraulica;

    public Tractor(boolean usoAgricola, boolean transimisionHidraulica, String Marca, String Modelo, int AñoFabricacion, double Precio) {
        super(Marca, Modelo, AñoFabricacion, Precio);
        this.usoAgricola = usoAgricola;
        this.transimisionHidraulica = transimisionHidraulica;
        super.setMotorGasolina(false);
        super.setNumeroLlantas(4);
    }

    public boolean isUsoAgricola() {
        return usoAgricola;
    }

    public void setUsoAgricola(boolean usoAgricola) {
        this.usoAgricola = usoAgricola;
    }

    public boolean isTransimisionHidraulica() {
        return transimisionHidraulica;
    }

    public void setTransimisionHidraulica(boolean transimisionHidraulica) {
        this.transimisionHidraulica = transimisionHidraulica;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.usoAgricola ? 1 : 0);
        hash = 23 * hash + (this.transimisionHidraulica ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tractor other = (Tractor) obj;
        if (this.usoAgricola != other.usoAgricola) {
            return false;
        }
        if (this.transimisionHidraulica != other.transimisionHidraulica) {
            return false;
        }
        return true;
    }

    public String visorClientes() {
        return "Tractor{" + super.visorClientesV() + "}";
    }

    @Override
    public String toString() {
        return "Tractor{" + super.toString() + "Numero de Llantas=" + super.getNumeroLlantas() + "usoAgricola=" + usoAgricola + ", transimisionHidraulica=" + transimisionHidraulica + '}';
    }

    @Override
    public String datosMantenimiento() {
        return "Tractor{" + super.visorClientesM() + "}";
    }

}
